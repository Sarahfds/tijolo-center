﻿using BlocoCenterLogin.DB;
using BlocoCenterLogin.DTOS;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.Cliente
{
    class ClienteDatabase
    {
        public int Salvar(ClienteDTO dto)
        {
            string script = @" INSERT INTO tb_cliente (nm_cliente, ds_telefone, ds_rg, ds_identificacao, tp_pessoa) VALUES (@nm_cliente, @ds_telefone, @ds_rg, @ds_identificacao, @tp_pessoa)";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_telefone", dto.telefone));
            parms.Add(new MySqlParameter("nm_cliente", dto.nome));
            parms.Add(new MySqlParameter("ds_rg", dto.rg));
            parms.Add(new MySqlParameter("ds_identificacao", dto.identificacao));
            parms.Add(new MySqlParameter("tp_pessoa", dto.pessoa));




            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }


        public void Alterar(ClienteDTO dto)
        {
            string script = @"UPDATE tb_cliente 
                                 SET nm_cliente = @nm_cliente,
                                     ds_telefone = @ds_telefone,
                                     ds_rg = @ds_rg,
                                     ds_identificacao = @ds_identificacao,
                                     tp_pessoa = @tp_pessoa
                                     WHERE id_cliente = @id_cliente"; 

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", dto.id_cliente));
            parms.Add(new MySqlParameter("nm_cliente", dto.nome));
            parms.Add(new MySqlParameter("ds_telefone", dto.telefone));
            parms.Add(new MySqlParameter("ds_rg", dto.rg));
            parms.Add(new MySqlParameter("ds_identificacao", dto.identificacao));
            parms.Add(new MySqlParameter("tp_pessoa", dto.pessoa));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);


        }
        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_cliente WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ClienteDTO> Consultar(string cliente)
        {
            string script = @"SELECT * FROM tb_cliente WHERE nm_cliente like @nm_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", cliente + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lista = new List<ClienteDTO>();
            while (reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();
                dto.id_cliente = reader.GetInt32("id_cliente");
                dto.nome = reader.GetString("nm_cliente");
                dto.identificacao = reader.GetString("ds_identificacao");
                dto.telefone = reader.GetString("ds_telefone");
                dto.pessoa = reader.GetString("tp_pessoa");
                dto.rg = reader.GetString("ds_rg");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        public List<ClienteDTO> Listar()
        {
            string script = "select * from tb_cliente";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> listar = new List<ClienteDTO>();

            while (reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();
                dto.id_cliente = reader.GetInt32("id_cliente");
                dto.telefone = reader.GetString("ds_telefone");
                dto.pessoa = reader.GetString("tp_pessoa");
                dto.rg = reader.GetString("ds_rg");
                dto.identificacao = reader.GetString("ds_identificacao");
                dto.nome = reader.GetString("nm_cliente");

                listar.Add(dto);


            }
            reader.Close();
            return listar;

        }
    }
}
