﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.FluxodeCaixa
{
    public class FluxoCaixaView
    {
        public DateTime Referencia { get; set; }
        public double Despesas { get; set; }
        public decimal Ganhos { get; set; }


    }
}
