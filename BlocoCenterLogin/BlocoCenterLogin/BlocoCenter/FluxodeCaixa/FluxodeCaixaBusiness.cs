﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.FluxodeCaixa
{
    class FluxodeCaixaBusiness
    {
        public List<FluxoCaixaView> Consultar(string datainicio, string datafim)
        {
            FluxodeCaixaDatabase pedidoDatabase = new FluxodeCaixaDatabase();
            return pedidoDatabase.Consultar(datainicio,datafim);
        }
    }
}
