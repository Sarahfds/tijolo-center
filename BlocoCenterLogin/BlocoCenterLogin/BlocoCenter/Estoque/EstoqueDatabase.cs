﻿using BlocoCenterLogin.DB;
using BlocoCenterLogin.DTOS;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.Estoque
{
    class EstoqueDatabase
    {
        public int Salvar (EstoqueDTO dto)
        {
            string script = @" INSERT INTO tb_estoque (id_produtofornecedor, ds_quantidade) VALUES (@id_produtofornecedor, @ds_quantidade)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produtofornecedor", dto.id_produto));
            parms.Add(new MySqlParameter("ds_quantidade", dto.quantidade));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public int DarBaixaEstoque(EstoqueDTO dto)
        {
            string script = @" UPDATE tb_estoque 
                                  SET ds_quantidade = ds_quantidade - ds_quantidade@ 
                                WHERE id_produtofornecedor = @id_produtofornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produtofornecedor", dto.id_produto));
            parms.Add(new MySqlParameter("ds_quantidade", dto.quantidade));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }


        public int AdicionarEstoque(EstoqueDTO dto)
        {
            string script = @" UPDATE tb_estoque 
                                  SET ds_quantidade = ds_quantidade + ds_quantidade@ 
                                WHERE id_produtofornecedor = @id_produtofornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produtofornecedor", dto.id_produto));
            parms.Add(new MySqlParameter("ds_quantidade", dto.quantidade));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
    }
}
