﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.DTOS
{
    class VendaDTO
    {
        public int id_venda { get; set; }

        public int quantidade { get; set; }

        public DateTime data { get; set; }

        public string Pagamento { get; set; }

        public int id_funcionario { get; set; }

        public int id_cliente { get; set; }

        public decimal precototal { get; set; }

    
    }
}
