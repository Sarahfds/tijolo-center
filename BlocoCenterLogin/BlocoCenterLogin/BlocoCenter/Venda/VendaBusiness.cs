﻿using BlocoCenterLogin.BlocoCenter.PedidoItemVendas;
using BlocoCenterLogin.BlocoCenter.Produto.ProdutoFornecedor;
using BlocoCenterLogin.BlocoCenter.Venda;
using BlocoCenterLogin.DTOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter
{
     class VendaBusiness
    {
        public int Salvar(VendaDTO venda, List<ProdutoDTO> produtos)
        {

            VendaDatabase vendaDatabase = new VendaDatabase();
            int idVenda = vendaDatabase.Salvar(venda);


            if (venda.quantidade <= 0)
            {
                throw new ArgumentException("Quantidade é obrigatório");
            }

            


            PedidoItemVendasBusiness itemBusiness = new PedidoItemVendasBusiness();

            foreach (ProdutoDTO itemm in produtos)
            {
                PedidoItemVendasDTO itemDto = new PedidoItemVendasDTO();
                itemDto.id_venda = idVenda;
                itemDto.id_produto = itemm.id_produto;

                itemBusiness.Salvar(itemDto);
            }

            return idVenda;

        }
    
        public List<ProdutoConsultarView> Consultar(string pedido)
        {
           VendaDatabase pedidoDatabase = new VendaDatabase();
            return pedidoDatabase.Consultar(pedido);
        }

        public void Remover(int id)
        {
            VendaDatabase db = new VendaDatabase();
            db.Remover(id);
        }

    }
}
