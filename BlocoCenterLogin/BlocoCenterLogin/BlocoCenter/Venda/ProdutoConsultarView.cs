﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.Produto.ProdutoFornecedor
{
    public class ProdutoConsultarView
    {
        public int ID { get; set; }
        public string Funcionario { get; set; }
        public string Pagamento { get; set; }
        public DateTime Data { get; set; }
        public decimal Total { get; set; }
        public string Cliente { get; set; }
        public int QtdItens { get; set; }
    }
}
