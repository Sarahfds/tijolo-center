﻿using BlocoCenterLogin.DB;
using BlocoCenterLogin.DTOS;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.Fornecedor
{
    class FornecedorDatabase
    {
        public int Salvar(FornecedorDTO dto)
        {
            string script = @" INSERT INTO tb_fornecedor (nm_fornecedor, ds_cnpj, ds_telefone, ds_cep, ds_email, ds_representante, ds_rg, ds_numerorua) VALUES (@nm_fornecedor, @ds_cnpj, @ds_telefone, @ds_cep, @ds_email, @ds_representante, @ds_rg, @ds_numerorua)";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_fornecedor", dto.nome));
            parms.Add(new MySqlParameter("ds_cnpj", dto.cnpj));
            parms.Add(new MySqlParameter("ds_telefone", dto.telefone));
            parms.Add(new MySqlParameter("ds_cep", dto.cep));
            parms.Add(new MySqlParameter("ds_email", dto.email));
            parms.Add(new MySqlParameter("ds_representante", dto.representante));
            parms.Add(new MySqlParameter("ds_numerorua", dto.numerorua));
            parms.Add(new MySqlParameter("ds_rg", dto.rg));




            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }


        public void Alterar(FornecedorDTO dto)
        {
            string script = @"UPDATE tb_fornecedor 
                                 SET nm_fornecedor = @nm_fornecedor,
                                     ds_telefone = @ds_telefone,
                                     ds_cep = @ds_cep,
                                     ds_email = @ds_email,
                                     ds_cnpj = @ds_cnpj,
                                     ds_numerorua = @ds_numerorua,
                                     ds_rg = @ds_rg,
                                     ds_representante = @ds_representante
                                     WHERE id_fornecedor = @id_fornecedor"; 


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_fornecedor", dto.id_fornecedor));
            parms.Add(new MySqlParameter("nm_fornecedor", dto.nome));
            parms.Add(new MySqlParameter("ds_telefone", dto.telefone));
            parms.Add(new MySqlParameter("ds_rg", dto.rg));
            parms.Add(new MySqlParameter("ds_representante", dto.representante));
            parms.Add(new MySqlParameter("ds_cep", dto.cep));
            parms.Add(new MySqlParameter("ds_email", dto.email));
            parms.Add(new MySqlParameter("ds_numerorua", dto.numerorua));
            parms.Add(new MySqlParameter("ds_cnpj", dto.cnpj));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);


        }
        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_fornecedor WHERE id_fornecedor = @id_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_fornecedor", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<FornecedorDTO> Consultar(string fornecedor)
        {
            string script = @"SELECT * FROM tb_fornecedor WHERE nm_fornecedor like @nm_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_fornecedor", fornecedor + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorDTO> lista = new List<FornecedorDTO>();
            while (reader.Read())
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.id_fornecedor = reader.GetInt32("id_fornecedor");
                dto.nome = reader.GetString("nm_fornecedor");
                dto.representante = reader.GetString("ds_representante");
                dto.telefone = reader.GetString("ds_telefone");
                dto.cep = reader.GetString("ds_cep");
                dto.rg = reader.GetString("ds_rg");
                dto.cnpj = reader.GetString("ds_cnpj");
                dto.numerorua = reader.GetString("ds_numerorua");
                dto.email = reader.GetString("ds_email");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        public List<FornecedorDTO> Listar()
        {
            string script = "select * from tb_fornecedor";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorDTO> listar = new List<FornecedorDTO>();

            while (reader.Read())
            {
                FornecedorDTO dto = new FornecedorDTO();
                
                dto.id_fornecedor = reader.GetInt32("id_fornecedor");
                dto.nome = reader.GetString("nm_fornecedor");
                dto.representante = reader.GetString("ds_representante");
                dto.telefone = reader.GetString("ds_telefone");
                dto.cep = reader.GetString("ds_cep");
                dto.rg = reader.GetString("ds_rg");
                dto.cnpj = reader.GetString("ds_cnpj");
                dto.numerorua = reader.GetString("ds_numerorua");
                dto.email = reader.GetString("ds_email");

                listar.Add(dto);


            }
            reader.Close();
            return listar;

        }
    }
}
