﻿using BlocoCenterLogin.DB;
using BlocoCenterLogin.DTOS;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.PedidoItemVendas
{
    public class PedidoItemVendasDatabase
    {
        public int Salvar(PedidoItemVendasDTO dto)
        {
            string script =  @"INSERT INTO tb_pedidoitemvendas (id_venda,id_produto) VALUES (@id_venda, @id_produto)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_venda", dto.id_venda));
            parms.Add(new MySqlParameter("id_produto", dto.id_produto));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);


        }

   
        public List<PedidoItemVendasDTO> ConsultarPorPedido(int idVenda)
        {
            string script = @"SELECT * FROM tb_pedidoitemvendas WHERE id_pedido = @id_pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido", idVenda));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoItemVendasDTO> lista = new List<PedidoItemVendasDTO>();
            while (reader.Read())
            {
                PedidoItemVendasDTO dto = new PedidoItemVendasDTO();
                dto.id_pedidoitemvendas = reader.GetInt32("id_pedidoitemvendas");
                dto.id_produto = reader.GetInt32("id_produto");
                dto.id_venda = reader.GetInt32("id_venda");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
