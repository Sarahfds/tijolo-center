﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.Funcionario
{
    public class FuncionarioDTO
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string CPF { get; set; }
        public string Senha { get; set; }
        public string Sexo { get; set; }
        public DateTime Nascimento { get; set; }
        public string RG { get; set; }
        public string CEP { get; set; }
        public string Telefone { get; set; }

        public string NumeroCasa { get; set; }
        public string Email { get; set; }
        public DateTime Admissao { get; set; }
        public string Cargo { get; set; }
        public bool permissao_adm { get; set; }
        public bool permissao_logistica { get; set; }
        public bool permissao_financeiro { get; set; }
        public bool permissao_rh { get; set; }
        public bool permissao_vendas { get; set; }
        public bool permissao_compras { get; set; }
        public decimal Salario { get; set; }












    }
}
