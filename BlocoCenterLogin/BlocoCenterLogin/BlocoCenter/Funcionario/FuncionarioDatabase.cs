﻿using BlocoCenterLogin.DB;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.Funcionario
{
    class FuncionarioDatabase
    {
        public FuncionarioDTO Logar(string cpf, string senha)
        {
            string script = @"SELECT * FROM tb_funcionario WHERE ds_cpf = @ds_cpf AND ds_senha = @ds_senha";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_cpf", cpf));
            parms.Add(new MySqlParameter("ds_senha", senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            FuncionarioDTO funcionario = null;

            if (reader.Read())
            {
                funcionario = new FuncionarioDTO();
                funcionario.ID = reader.GetInt32("id_funcionario");
                funcionario.Nome = reader.GetString("nm_funcionario");
                funcionario.CPF = reader.GetString("ds_cpf");
                funcionario.RG = reader.GetString("ds_rg");
                funcionario.Salario = reader.GetInt32("ds_salario");
                funcionario.Senha = reader.GetString("ds_senha");
                funcionario.Sexo = reader.GetString("ds_sexo");
                funcionario.Email = reader.GetString("ds_email");
                funcionario.Nascimento = reader.GetDateTime("dt_Nascimento");
                funcionario.NumeroCasa = reader.GetString("ds_numerocasa");
                funcionario.Admissao = reader.GetDateTime("dt_admissao");
                funcionario.permissao_adm = reader.GetBoolean("bt_permissao_adm");
                funcionario.permissao_compras = reader.GetBoolean("bt_permissao_compras");
                funcionario.permissao_financeiro = reader.GetBoolean("bt_permissao_financeiro");
                funcionario.permissao_logistica = reader.GetBoolean("bt_permissao_logistica");
                funcionario.permissao_rh = reader.GetBoolean("bt_permissao_rh");
                funcionario.permissao_vendas = reader.GetBoolean("bt_permissao_vendas");
            }

            reader.Close();

            return funcionario;
        }

        public int Salvar(FuncionarioDTO dto)
        {
            string script = @" INSERT INTO tb_funcionario (nm_funcionario, ds_sexo, dt_nascimento, ds_rg, ds_cpf, ds_telefone,	ds_cep, ds_numerocasa, ds_email, dt_admissao, ds_cargo, bt_permissao_adm, bt_permissao_logistica, bt_permissao_financeiro, bt_permissao_rh, bt_permissao_vendas, bt_permissao_compras, ds_salario, ds_senha) VALUES (@nm_funcionario, @ds_sexo, @dt_nascimento, @ds_rg, @ds_cpf, @ds_telefone, @ds_cep, @ds_numerocasa, @ds_email, @dt_admissao, @ds_cargo, @bt_permissao_adm, @bt_permissao_logistica, @bt_permissao_financeiro, @bt_permissao_rh, @bt_permissao_vendas, @bt_permissao_compras, @ds_salario, @ds_senha)";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", dto.Nome));
             parms.Add(new MySqlParameter("ds_numerocasa", dto.NumeroCasa));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_sexo", dto.Sexo));
            parms.Add(new MySqlParameter("dt_nascimento", dto.Nascimento));
            parms.Add(new MySqlParameter("ds_rg", dto.RG));
            parms.Add(new MySqlParameter("ds_cep", dto.CEP));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parms.Add(new MySqlParameter("ds_senha", dto.Senha));
            parms.Add(new MySqlParameter("dt_admissao", dto.Admissao));
            parms.Add(new MySqlParameter("ds_cargo", dto.Cargo));
            parms.Add(new MySqlParameter("bt_permissao_adm", dto.permissao_adm));
            parms.Add(new MySqlParameter("bt_permissao_logistica", dto.permissao_logistica));
            parms.Add(new MySqlParameter("bt_permissao_financeiro", dto.permissao_financeiro));
            parms.Add(new MySqlParameter("bt_permissao_rh", dto.permissao_rh));
            parms.Add(new MySqlParameter("bt_permissao_vendas", dto.permissao_vendas));
            parms.Add(new MySqlParameter("bt_permissao_compras", dto.permissao_compras));
            parms.Add(new MySqlParameter("ds_salario", dto.Salario));
            

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        
        public void Alterar(FuncionarioDTO dto)
        {
            string script = @"UPDATE tb_funcionario 
                                 SET nm_funcionario  = @nm_funcionario,
                                     ds_cpf = @ds_cpf,
                                     dt_admissao = @dt_admissao,
                                     ds_cep = @ds_cep,
                                     ds_numerocasa = @ds_numerocasa,
                                     ds_cargo = @ds_cargo,
                                     ds_rg = @ds_rg,
                                     ds_senha = @ds_senha,
                                     ds_telefone = @ds_telefone,
                                     ds_email = @ds_email,
                                     dt_nascimento = @dt_nascimento,
                                     ds_sexo = @ds_sexo,
                                     bt_permissao_adm = @bt_permissao_adm,
                                     bt_permissao_logistica = @bt_permissao_logistica,
                                     bt_permissao_financeiro = @bt_permissao_financeiro,
                                     bt_permissao_rh = @bt_permissao_rh,
                                     bt_permissao_vendas = @bt_permissao_vendas,
                                     bt_permissao_compras = @bt_permissao_compras,
                                     ds_salario = @ds_salario
                                     WHERE id_funcionario = @id_funcionario";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", dto.ID));
            parms.Add(new MySqlParameter("nm_funcionario", dto.Nome));
            parms.Add(new MySqlParameter("ds_numerocasa", dto.NumeroCasa));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_sexo", dto.Sexo));
            parms.Add(new MySqlParameter("dt_nascimento", dto.Nascimento));
            parms.Add(new MySqlParameter("ds_rg", dto.RG));
            parms.Add(new MySqlParameter("ds_cep", dto.CEP));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parms.Add(new MySqlParameter("ds_senha", dto.Senha));
            parms.Add(new MySqlParameter("dt_admissao", dto.Admissao));
            parms.Add(new MySqlParameter("ds_cargo", dto.Cargo));
            parms.Add(new MySqlParameter("bt_permissao_adm", dto.permissao_adm));
            parms.Add(new MySqlParameter("bt_permissao_logistica", dto.permissao_logistica));
            parms.Add(new MySqlParameter("bt_permissao_financeiro", dto.permissao_financeiro));
            parms.Add(new MySqlParameter("bt_permissao_rh", dto.permissao_rh));
            parms.Add(new MySqlParameter("bt_permissao_vendas", dto.permissao_vendas));
            parms.Add(new MySqlParameter("bt_permissao_compras", dto.permissao_compras));
            parms.Add(new MySqlParameter("ds_salario", dto.Salario));


            Database db = new Database();
            db.ExecuteInsertScript(script, parms);


        }
        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_funcionario WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<FuncionarioDTO> Consultar(string funcionario)
        {
            string script = @"SELECT * FROM tb_funcionario WHERE nm_funcionario like @nm_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", funcionario + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.ID = reader.GetInt32("id_funcionario");
                dto.Nome = reader.GetString("nm_funcionario");
                dto.CEP = reader.GetString("ds_cep");
                dto.NumeroCasa = reader.GetString("ds_numerocasa");
                dto.Telefone = reader.GetString("ds_cep");
                dto.Sexo = reader.GetString("ds_sexo");
                dto.RG = reader.GetString("ds_rg");
                dto.CPF = reader.GetString("ds_cpf");
                dto.Senha = reader.GetString("ds_senha");
                dto.Nascimento = reader.GetDateTime("dt_Nascimento");
                dto.Salario = reader.GetInt32("ds_salario");
                dto.Email = reader.GetString("ds_email");
                dto.Cargo = reader.GetString("ds_cargo");
                dto.Admissao = reader.GetDateTime("dt_admissao");
                dto.permissao_adm = reader.GetBoolean("bt_permissao_adm");
                dto.permissao_compras = reader.GetBoolean("bt_permissao_compras");
                dto.permissao_financeiro = reader.GetBoolean("bt_permissao_financeiro");
                dto.permissao_logistica = reader.GetBoolean("bt_permissao_logistica");
                dto.permissao_rh = reader.GetBoolean("bt_permissao_rh");
                dto.permissao_vendas = reader.GetBoolean("bt_permissao_vendas");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        public List<FuncionarioDTO> Listar()
        {
            string script = "select * from tb_funcionario";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> listar = new List<FuncionarioDTO>();

            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.ID = reader.GetInt32("id_funcionario");
                dto.Nome = reader.GetString("nm_funcionario");
                dto.CEP = reader.GetString("ds_cep");
                dto.NumeroCasa = reader.GetString("ds_numerocasa");
                dto.Telefone = reader.GetString("ds_cep");
                dto.Sexo = reader.GetString("ds_sexo");
                dto.Cargo = reader.GetString("ds_cargo");
                dto.RG = reader.GetString("ds_rg");
                dto.CPF = reader.GetString("ds_cpf");
                dto.Senha = reader.GetString("ds_senha");
                dto.Nascimento = reader.GetDateTime("dt_Nascimento");
                dto.Salario = reader.GetInt32("ds_salario");
                dto.Email = reader.GetString("ds_email");
                dto.Admissao = reader.GetDateTime("dt_admissao");
                dto.permissao_adm = reader.GetBoolean("bt_permissao_adm");
                dto.permissao_compras = reader.GetBoolean("bt_permissao_compras");
                dto.permissao_financeiro = reader.GetBoolean("bt_permissao_financeiro");
                dto.permissao_logistica = reader.GetBoolean("bt_permissao_logistica");
                dto.permissao_rh = reader.GetBoolean("bt_permissao_rh");
                dto.permissao_vendas = reader.GetBoolean("bt_permissao_vendas");


                listar.Add(dto);
            


            }
            reader.Close();
            return listar;

        }
    }
    }


