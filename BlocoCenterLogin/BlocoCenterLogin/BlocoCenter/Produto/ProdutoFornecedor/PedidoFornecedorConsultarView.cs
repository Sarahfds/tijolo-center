﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.Produto.ProdutoFornecedor
{
    class PedidoFornecedorConsultarView
    {
        public int ID { get; set; }
        public string Fornecedor { get; set; }
        public string Pagamento { get; set; }
        public DateTime Data { get; set; }
        public decimal Total { get; set; }
        public int QtdItens { get; set; }
       
    }
}
