﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.Produto.ProdutoFornecedor
{
    public class ProdutoFornecedorDTO
    {
        public int ID { get; set; }
        public string nome { get; set; }

        public decimal preco { get; set; }
    }
}

