﻿using BlocoCenterLogin.DB;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.Produto.ProdutoFornecedor
{
    class ProdutoFornecedorDatabase
    {
        public int Salvar(ProdutoFornecedorDTO dto)
        {
            string script = @" INSERT INTO tb_produtofornecedor (nm_produtof, vl_precof) VALUES (@nm_produtof, @vl_precof)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produtof", dto.nome));
            parms.Add(new MySqlParameter("vl_precof", dto.preco));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }


        public void Alterar(ProdutoFornecedorDTO dto)
        {
            string script = @"UPDATE tb_produtofornecedor 
                                 SET nm_produtof = @nm_produtof,
                                     vl_precof = @vl_precof
                                     WHERE id_produtofornecedor = @id_produtofornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produtofornecedor", dto.ID));
            parms.Add(new MySqlParameter("nm_produtof", dto.nome));
            parms.Add(new MySqlParameter("vl_precof", dto.preco));
          


            Database db = new Database();
            db.ExecuteInsertScript(script, parms);


        }
        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_produtofornecedor WHERE id_produtofornecedor = @id_produtofornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produtofornecedor", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<ProdutoFornecedorDTO> Consultar(string produto)
        {
            string script = @"SELECT * FROM tb_produtofornecedor WHERE nm_produtof like @nm_produtof";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produtof", produto + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoFornecedorDTO> lista = new List<ProdutoFornecedorDTO>();
            while (reader.Read())
            {
                ProdutoFornecedorDTO dto = new ProdutoFornecedorDTO();
                dto.ID = reader.GetInt32("id_produtofornecedor");
                dto.nome = reader.GetString("nm_produtof");
                dto.preco = reader.GetInt32("vl_precof");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        public List<ProdutoFornecedorDTO> Listar()
        {
            string script = "select * from tb_produtofornecedor";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoFornecedorDTO> listar = new List<ProdutoFornecedorDTO>();

            while (reader.Read())
            {
                ProdutoFornecedorDTO dto = new ProdutoFornecedorDTO();
                dto.ID = reader.GetInt32("id_produtofornecedor");
                dto.nome = reader.GetString("nm_produtof");
                dto.preco = reader.GetInt32("vl_precof");
                listar.Add(dto);


            }
            reader.Close();
            return listar;

        }
    }
}
