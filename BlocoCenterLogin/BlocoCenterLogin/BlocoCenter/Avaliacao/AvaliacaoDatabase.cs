﻿using BlocoCenterLogin.DB;
using BlocoCenterLogin.DTOS;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.Avaliacao
{
    class AvaliacaoDatabase
    {
        public int Salvar(AvaliacaoDTO dto)
        {
            string script = @" INSERT INTO tb_avaliacao (nm_avaliador, id_funcionario, dt_avaliacao, ds_observacao, ds_desempenho) VALUES (@nm_avaliador, @id_funcionario, @dt_avaliacao, @ds_observacao, @ds_desempenho)";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_avaliador", dto.avaliador));
            parms.Add(new MySqlParameter("id_funcionario", dto.id_funcionario));
            parms.Add(new MySqlParameter("dt_avaliacao", dto.DataAvaliacao));
            parms.Add(new MySqlParameter("ds_observacao", dto.observacao));
            parms.Add(new MySqlParameter("ds_desempenho", dto.Desempenho));



            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }



        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_avaliacao WHERE id_avaliacao = @id_avaliacao";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<AvaliacaoDTO> Consultar(string avaliador)
        {
            string script = @"SELECT * FROM tb_avaliacao WHERE nm_avaliador like @nm_avaliador";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_avaliador", avaliador + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<AvaliacaoDTO> lista = new List<AvaliacaoDTO>();
            while (reader.Read())
            {
                AvaliacaoDTO dto = new AvaliacaoDTO();
                dto.id_avaliacao = reader.GetInt32("id_avaliacao");
                dto.avaliador = reader.GetString("nm_avaliador");
                dto.DataAvaliacao = reader.GetDateTime("dt_avaliacao");
                dto.id_funcionario = reader.GetInt32("id_funcionario");
                dto.observacao = reader.GetString("ds_observacao");
                dto.Desempenho = reader.GetString("ds_desempenho");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        public List<AvaliacaoDTO> Listar()
        {
            string script = "select * from tb_avaliacao";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<AvaliacaoDTO> listar = new List<AvaliacaoDTO>();

            while (reader.Read())
            {
                AvaliacaoDTO dto = new AvaliacaoDTO();
                dto.id_avaliacao = reader.GetInt32("id_avaliacao");
                dto.avaliador = reader.GetString("nm_avaliador");
                dto.DataAvaliacao = reader.GetDateTime("dt_avaliacao");
                dto.id_funcionario = reader.GetInt32("id_funcionario");
                dto.observacao = reader.GetString("ds_observacao");
                dto.Desempenho = reader.GetString("ds_desempenho");

                listar.Add(dto);

            }
            reader.Close();
            return listar;


        }
    }
}
    