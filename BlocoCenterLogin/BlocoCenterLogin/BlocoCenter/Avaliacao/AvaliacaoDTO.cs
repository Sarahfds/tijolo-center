﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.DTOS
{
    class AvaliacaoDTO
    {
        public int id_avaliacao { get; set; }
        public string avaliador { get; set; }
        public int id_funcionario { get; set; }
        public string observacao { get; set; }
        public string Desempenho { get; set; }
        public DateTime DataAvaliacao { get; set; }

    }
}
