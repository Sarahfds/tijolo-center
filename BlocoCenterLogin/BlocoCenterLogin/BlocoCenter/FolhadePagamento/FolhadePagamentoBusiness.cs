﻿using BlocoCenterLogin.DTOS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.BlocoCenter.FolhadePagamento
{
    class FolhadePagamentoBusiness
    {
        public int Salvar(FolhadePagamentoDTO dto)
        {
            

           
            FolhadePagamentoDatabase db = new FolhadePagamentoDatabase();
            return db.Salvar(dto);
        }


        public List<FolhadePagamentoDTO> Consultar(string folhadepagamento)
        {
            FolhadePagamentoDatabase db = new FolhadePagamentoDatabase();
            return db.Consultar(folhadepagamento);
        }

        public void Remover(int id)
        {
            FolhadePagamentoDatabase db = new FolhadePagamentoDatabase();
            db.Remover(id);
        }
    }
}
