﻿using BlocoCenterLogin.BlocoCenter.Produto.ProdutoFornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin.Telas.Logistica
{
    public partial class BlocoCenterAlterarProdutoF : Form
    {
        public BlocoCenterAlterarProdutoF()
        {
            InitializeComponent();
        }

        ProdutoFornecedorDTO  ProdutoFornecedor;

        public void LoadScreenProduto(ProdutoFornecedorDTO ProdutoF)
        {
            this.ProdutoFornecedor = ProdutoF;

            lbl0.Text = ProdutoF.ID.ToString();


          
            txtpreco.Text = ProdutoF.preco.ToString();
            txtproduto.Text = ProdutoF.nome;
        

        }
        private void btnsalvar_Click(object sender, EventArgs e)
        {
            this.ProdutoFornecedor.nome = txtproduto.Text.Trim();
            this.ProdutoFornecedor.preco = Convert.ToDecimal(txtpreco.Text.Trim());
         

            ProdutoFornecedorBusiness business = new ProdutoFornecedorBusiness();
            business.Alterar(ProdutoFornecedor);
            MessageBox.Show("Produto alterado com sucesso");
        }

        private void btnsair_Click(object sender, EventArgs e)
        {
            BlocoCenterCadastroProdutoFornecedor newForm2 = new BlocoCenterCadastroProdutoFornecedor();
            this.Hide();
            newForm2.ShowDialog();

        }
    }
}
