﻿namespace BlocoCenterLogin.Telas.Logistica
{
    partial class BlocoCenterAlterarFornecedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BlocoCenterAlterarFornecedor));
            this.lblnumerorua = new System.Windows.Forms.Label();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.txtcep = new System.Windows.Forms.TextBox();
            this.txtemail = new System.Windows.Forms.TextBox();
            this.txtrepresentante = new System.Windows.Forms.TextBox();
            this.txtnumerorua = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblemail = new System.Windows.Forms.Label();
            this.lblrg = new System.Windows.Forms.Label();
            this.lblrepresentante = new System.Windows.Forms.Label();
            this.btnsair = new System.Windows.Forms.Button();
            this.btnalterar = new System.Windows.Forms.Button();
            this.lblnome = new System.Windows.Forms.Label();
            this.lblcnpj = new System.Windows.Forms.Label();
            this.lbltelefone = new System.Windows.Forms.Label();
            this.lblcep = new System.Windows.Forms.Label();
            this.lblcodigofornecedor = new System.Windows.Forms.Label();
            this.lbl0 = new System.Windows.Forms.Label();
            this.txtCNPJ = new System.Windows.Forms.MaskedTextBox();
            this.txtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.txtRG = new System.Windows.Forms.MaskedTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblnumerorua
            // 
            this.lblnumerorua.AutoSize = true;
            this.lblnumerorua.BackColor = System.Drawing.Color.Transparent;
            this.lblnumerorua.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnumerorua.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblnumerorua.Location = new System.Drawing.Point(479, 280);
            this.lblnumerorua.Name = "lblnumerorua";
            this.lblnumerorua.Size = new System.Drawing.Size(168, 30);
            this.lblnumerorua.TabIndex = 156;
            this.lblnumerorua.Text = "Numero da rua:";
            // 
            // txtnome
            // 
            this.txtnome.Location = new System.Drawing.Point(650, 90);
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(374, 20);
            this.txtnome.TabIndex = 1;
            // 
            // txtcep
            // 
            this.txtcep.Location = new System.Drawing.Point(650, 175);
            this.txtcep.Name = "txtcep";
            this.txtcep.Size = new System.Drawing.Size(374, 20);
            this.txtcep.TabIndex = 4;
            // 
            // txtemail
            // 
            this.txtemail.Location = new System.Drawing.Point(650, 204);
            this.txtemail.Name = "txtemail";
            this.txtemail.Size = new System.Drawing.Size(374, 20);
            this.txtemail.TabIndex = 5;
            // 
            // txtrepresentante
            // 
            this.txtrepresentante.Location = new System.Drawing.Point(650, 233);
            this.txtrepresentante.Name = "txtrepresentante";
            this.txtrepresentante.Size = new System.Drawing.Size(374, 20);
            this.txtrepresentante.TabIndex = 6;
            // 
            // txtnumerorua
            // 
            this.txtnumerorua.Location = new System.Drawing.Point(650, 290);
            this.txtnumerorua.Name = "txtnumerorua";
            this.txtnumerorua.Size = new System.Drawing.Size(374, 20);
            this.txtnumerorua.TabIndex = 8;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(45, 50);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(400, 256);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 145;
            this.pictureBox1.TabStop = false;
            // 
            // lblemail
            // 
            this.lblemail.AutoSize = true;
            this.lblemail.BackColor = System.Drawing.Color.Transparent;
            this.lblemail.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblemail.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblemail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblemail.Location = new System.Drawing.Point(567, 194);
            this.lblemail.Name = "lblemail";
            this.lblemail.Size = new System.Drawing.Size(80, 30);
            this.lblemail.TabIndex = 141;
            this.lblemail.Text = "E-mail:";
            // 
            // lblrg
            // 
            this.lblrg.AutoSize = true;
            this.lblrg.BackColor = System.Drawing.Color.Transparent;
            this.lblrg.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblrg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblrg.Location = new System.Drawing.Point(599, 252);
            this.lblrg.Name = "lblrg";
            this.lblrg.Size = new System.Drawing.Size(48, 30);
            this.lblrg.TabIndex = 139;
            this.lblrg.Text = "RG:";
            // 
            // lblrepresentante
            // 
            this.lblrepresentante.AutoSize = true;
            this.lblrepresentante.BackColor = System.Drawing.Color.Transparent;
            this.lblrepresentante.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblrepresentante.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblrepresentante.Location = new System.Drawing.Point(488, 223);
            this.lblrepresentante.Name = "lblrepresentante";
            this.lblrepresentante.Size = new System.Drawing.Size(160, 30);
            this.lblrepresentante.TabIndex = 138;
            this.lblrepresentante.Text = "Representante:";
            // 
            // btnsair
            // 
            this.btnsair.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnsair.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsair.Font = new System.Drawing.Font("Goudy Old Style", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsair.ForeColor = System.Drawing.Color.White;
            this.btnsair.Location = new System.Drawing.Point(912, 489);
            this.btnsair.Name = "btnsair";
            this.btnsair.Size = new System.Drawing.Size(112, 45);
            this.btnsair.TabIndex = 10;
            this.btnsair.Text = "Voltar";
            this.btnsair.UseVisualStyleBackColor = false;
            this.btnsair.Click += new System.EventHandler(this.btnsair_Click);
            // 
            // btnalterar
            // 
            this.btnalterar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnalterar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnalterar.Font = new System.Drawing.Font("Goudy Old Style", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnalterar.ForeColor = System.Drawing.Color.White;
            this.btnalterar.Location = new System.Drawing.Point(745, 489);
            this.btnalterar.Name = "btnalterar";
            this.btnalterar.Size = new System.Drawing.Size(163, 45);
            this.btnalterar.TabIndex = 9;
            this.btnalterar.Text = "Alterar";
            this.btnalterar.UseVisualStyleBackColor = false;
            this.btnalterar.Click += new System.EventHandler(this.btnalterar_Click);
            // 
            // lblnome
            // 
            this.lblnome.AutoSize = true;
            this.lblnome.BackColor = System.Drawing.Color.Transparent;
            this.lblnome.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnome.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblnome.Location = new System.Drawing.Point(567, 80);
            this.lblnome.Name = "lblnome";
            this.lblnome.Size = new System.Drawing.Size(79, 30);
            this.lblnome.TabIndex = 166;
            this.lblnome.Text = "Nome:";
            this.lblnome.Click += new System.EventHandler(this.lblnome_Click);
            // 
            // lblcnpj
            // 
            this.lblcnpj.AutoSize = true;
            this.lblcnpj.BackColor = System.Drawing.Color.Transparent;
            this.lblcnpj.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcnpj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblcnpj.Location = new System.Drawing.Point(575, 109);
            this.lblcnpj.Name = "lblcnpj";
            this.lblcnpj.Size = new System.Drawing.Size(71, 30);
            this.lblcnpj.TabIndex = 167;
            this.lblcnpj.Text = "CNPJ:";
            // 
            // lbltelefone
            // 
            this.lbltelefone.AutoSize = true;
            this.lbltelefone.BackColor = System.Drawing.Color.Transparent;
            this.lbltelefone.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltelefone.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbltelefone.Location = new System.Drawing.Point(544, 137);
            this.lbltelefone.Name = "lbltelefone";
            this.lbltelefone.Size = new System.Drawing.Size(104, 30);
            this.lbltelefone.TabIndex = 168;
            this.lbltelefone.Text = "Telefone:";
            // 
            // lblcep
            // 
            this.lblcep.AutoSize = true;
            this.lblcep.BackColor = System.Drawing.Color.Transparent;
            this.lblcep.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcep.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblcep.Location = new System.Drawing.Point(590, 165);
            this.lblcep.Name = "lblcep";
            this.lblcep.Size = new System.Drawing.Size(56, 30);
            this.lblcep.TabIndex = 169;
            this.lblcep.Text = "CEP:";
            // 
            // lblcodigofornecedor
            // 
            this.lblcodigofornecedor.AutoSize = true;
            this.lblcodigofornecedor.BackColor = System.Drawing.Color.Transparent;
            this.lblcodigofornecedor.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcodigofornecedor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblcodigofornecedor.Location = new System.Drawing.Point(690, 50);
            this.lblcodigofornecedor.Name = "lblcodigofornecedor";
            this.lblcodigofornecedor.Size = new System.Drawing.Size(207, 30);
            this.lblcodigofornecedor.TabIndex = 171;
            this.lblcodigofornecedor.Text = "Código Fornecedor:";
            // 
            // lbl0
            // 
            this.lbl0.AutoSize = true;
            this.lbl0.BackColor = System.Drawing.Color.Transparent;
            this.lbl0.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl0.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbl0.Location = new System.Drawing.Point(892, 50);
            this.lbl0.Name = "lbl0";
            this.lbl0.Size = new System.Drawing.Size(25, 30);
            this.lbl0.TabIndex = 11;
            this.lbl0.Text = "0";
            // 
            // txtCNPJ
            // 
            this.txtCNPJ.Location = new System.Drawing.Point(650, 119);
            this.txtCNPJ.Mask = "00.000.000/0000-00";
            this.txtCNPJ.Name = "txtCNPJ";
            this.txtCNPJ.Size = new System.Drawing.Size(374, 20);
            this.txtCNPJ.TabIndex = 2;
            // 
            // txtTelefone
            // 
            this.txtTelefone.Location = new System.Drawing.Point(650, 147);
            this.txtTelefone.Mask = "(00)00000-0000";
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(374, 20);
            this.txtTelefone.TabIndex = 3;
            // 
            // txtRG
            // 
            this.txtRG.Location = new System.Drawing.Point(650, 262);
            this.txtRG.Mask = "00.000.000-0";
            this.txtRG.Name = "txtRG";
            this.txtRG.Size = new System.Drawing.Size(374, 20);
            this.txtRG.TabIndex = 7;
            // 
            // BlocoCenterAlterarFornecedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(251)))), ((int)(((byte)(242)))));
            this.ClientSize = new System.Drawing.Size(1079, 585);
            this.Controls.Add(this.txtRG);
            this.Controls.Add(this.txtTelefone);
            this.Controls.Add(this.txtCNPJ);
            this.Controls.Add(this.lbl0);
            this.Controls.Add(this.lblcodigofornecedor);
            this.Controls.Add(this.lblcep);
            this.Controls.Add(this.lbltelefone);
            this.Controls.Add(this.lblcnpj);
            this.Controls.Add(this.lblnome);
            this.Controls.Add(this.btnsair);
            this.Controls.Add(this.btnalterar);
            this.Controls.Add(this.lblnumerorua);
            this.Controls.Add(this.txtnome);
            this.Controls.Add(this.txtcep);
            this.Controls.Add(this.txtemail);
            this.Controls.Add(this.txtrepresentante);
            this.Controls.Add(this.txtnumerorua);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblemail);
            this.Controls.Add(this.lblrg);
            this.Controls.Add(this.lblrepresentante);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BlocoCenterAlterarFornecedor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Alterar Fornecedor";
            this.Load += new System.EventHandler(this.BlocoCenterAlterarFornecedor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblnumerorua;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.TextBox txtcep;
        private System.Windows.Forms.TextBox txtemail;
        private System.Windows.Forms.TextBox txtrepresentante;
        private System.Windows.Forms.TextBox txtnumerorua;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblemail;
        private System.Windows.Forms.Label lblrg;
        private System.Windows.Forms.Label lblrepresentante;
        private System.Windows.Forms.Button btnsair;
        private System.Windows.Forms.Button btnalterar;
        private System.Windows.Forms.Label lblnome;
        private System.Windows.Forms.Label lblcnpj;
        private System.Windows.Forms.Label lbltelefone;
        private System.Windows.Forms.Label lblcep;
        private System.Windows.Forms.Label lblcodigofornecedor;
        private System.Windows.Forms.Label lbl0;
        private System.Windows.Forms.MaskedTextBox txtCNPJ;
        private System.Windows.Forms.MaskedTextBox txtTelefone;
        private System.Windows.Forms.MaskedTextBox txtRG;
    }
}