﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin
{
    public partial class BlocoCenterLogistica : Form
    {
        public BlocoCenterLogistica()
        {
            InitializeComponent();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            BlocoCenterConsultaPedidoF newForm2 = new BlocoCenterConsultaPedidoF();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            BlocoCenterCadastroCliente newForm2 = new BlocoCenterCadastroCliente();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            BlocoCenterMenu newForm2 = new BlocoCenterMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            BlocoCenterMenu newForm2 = new BlocoCenterMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void btnpedido_Click(object sender, EventArgs e)
        {
            BlocoCenterConsultaPedidoF newForm2 = new BlocoCenterConsultaPedidoF();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            BlocoCenterCadastroCliente newForm2 = new BlocoCenterCadastroCliente();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void btnsair_Click(object sender, EventArgs e)
        {
            BlocoCenterMenu newForm2 = new BlocoCenterMenu();
            this.Hide();
            newForm2.ShowDialog();
        }
    }
}
