﻿using BlocoCenterLogin.BlocoCenter.Estoque;
using BlocoCenterLogin.BlocoCenter.Fornecedor;
using BlocoCenterLogin.BlocoCenter.PedidoFornecedor;
using BlocoCenterLogin.BlocoCenter.Produto.ProdutoFornecedor;
using BlocoCenterLogin.DTOS;
using BlocoCenterLogin.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin
{
    public partial class BlocoCenterPedidoFornecedor : Form
    {
        BindingList<ProdutoFornecedorDTO> Carrinho = new BindingList<ProdutoFornecedorDTO>();

    
        public BlocoCenterPedidoFornecedor()
        {
            InitializeComponent();
            CarregarCombo();
            CarregarDgv();

        }

        void CarregarCombo()
        {
            ProdutoFornecedorBusiness business = new ProdutoFornecedorBusiness();
            List<ProdutoFornecedorDTO> lista = business.Listar();

            cboPro.ValueMember = nameof(ProdutoFornecedorDTO.ID);
            cboPro.DisplayMember = nameof(ProdutoFornecedorDTO.nome);
            cboPro.DataSource = lista;


            FornecedorBusiness funbus = new FornecedorBusiness();
            List<FornecedorDTO> listinha = funbus.Listar();

            cboFor.ValueMember = nameof(FornecedorDTO.id_fornecedor);
            cboFor.DisplayMember = nameof(FornecedorDTO.nome);
            cboFor.DataSource = listinha;
        }

        void CarregarDgv()
        {
            dgvCarrinho.AutoGenerateColumns = false;
            dgvCarrinho.DataSource = Carrinho;
        }
        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ProdutoFornecedorDTO dto = cboPro.SelectedItem as ProdutoFornecedorDTO;

            int qtd = Convert.ToInt32(nudQuantidade.Text);

            for (int i = 0; i < qtd; i++)
            {
                Carrinho.Add(dto);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
           

            FornecedorDTO cat = cboFor.SelectedItem as FornecedorDTO;
            ProdutoFornecedorDTO forn = cboPro.SelectedItem as ProdutoFornecedorDTO;


            PedidoFornecedorDTO dto = new PedidoFornecedorDTO();
          
            dto.quantidade = Convert.ToInt32(nudQuantidade.Value);
            dto.Pagamento = cboFormadepagam.Text;
            dto.DataPagamento = DateTime.Now;
            dto.id_fornecedor = cat.id_fornecedor;

            PedidoFornecedorBusiness business = new PedidoFornecedorBusiness();
            business.Salvar(dto, Carrinho.ToList());

            MessageBox.Show("Pedido feito com sucesso.", "TijoloCenter", MessageBoxButtons.OK, MessageBoxIcon.Information);


            

        }

        private void button3_Click(object sender, EventArgs e)
        {
            BlocoCenterMenu newForm2 = new BlocoCenterMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void BlocoCenterPedidoFornecedor_Load(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            BlocoCenterMenu newForm2 = new BlocoCenterMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void btncarrinho_Click(object sender, EventArgs e)
        {
            ProdutoFornecedorDTO dto = cboPro.SelectedItem as ProdutoFornecedorDTO;

            int qtd = Convert.ToInt32(nudQuantidade.Text);

            for (int i = 0; i < qtd; i++)
            {
                Carrinho.Add(dto);
            }
        }

        private void btnpedido_Click(object sender, EventArgs e)
        {
            decimal totalcompras = Convert.ToDecimal(lbl0.Text);

            FornecedorDTO cat = cboFor.SelectedItem as FornecedorDTO;
            ProdutoFornecedorDTO forn = cboPro.SelectedItem as ProdutoFornecedorDTO;


            PedidoFornecedorDTO dto = new PedidoFornecedorDTO();

            dto.quantidade = Convert.ToInt32(nudQuantidade.Value);
            dto.Pagamento = cboFormadepagam.Text;
            dto.DataPagamento = DateTime.Now;
            dto.id_fornecedor = cat.id_fornecedor;
            dto.precototal = totalcompras;

            
            PedidoFornecedorBusiness business = new PedidoFornecedorBusiness();
            business.Salvar(dto, Carrinho.ToList());

            MessageBox.Show("Pedido feito com sucesso.", "TijoloCenter", MessageBoxButtons.OK, MessageBoxIcon.Information);


        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            ProdutoFornecedorDTO dto1 = new ProdutoFornecedorDTO();

            decimal produto = 0;
            int quantidade = Convert.ToInt32(nudQuantidade.Value);

            ProdutoFornecedorDTO forn = cboPro.SelectedItem as ProdutoFornecedorDTO;
            produto = forn.preco;

            decimal total = produto * quantidade;
            lbl0.Text = total.ToString();
        }

        private void lbl0_Click(object sender, EventArgs e)
        {

        }
    }
}
