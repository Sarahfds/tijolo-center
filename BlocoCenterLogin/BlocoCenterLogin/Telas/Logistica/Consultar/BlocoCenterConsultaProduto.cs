﻿using BlocoCenterLogin.BlocoCenter.Produto.ProdutoFornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin.Telas.Logistica
{
    public partial class BlocoCenterConsultaProduto : Form
    {
        public BlocoCenterConsultaProduto()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnsair_Click(object sender, EventArgs e)
        {
            BlocoCenterLogisticaMenu newForm2 = new BlocoCenterLogisticaMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void btnbuscar_Click(object sender, EventArgs e)
        {
            ProdutoFornecedorBusiness business = new ProdutoFornecedorBusiness();
            List<ProdutoFornecedorDTO> lista = business.Consultar(txtpesquisa.Text);



            dgvproduto.AutoGenerateColumns = false;
            dgvproduto.DataSource = lista;
        }

      
        

        private void dgvPro_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                ProdutoFornecedorDTO produ = dgvproduto.Rows[e.RowIndex].DataBoundItem as ProdutoFornecedorDTO;

                DialogResult r = MessageBox.Show("Deseja excluir o cadastro?", "Tijolo Center",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    ProdutoFornecedorBusiness business = new ProdutoFornecedorBusiness();
                    business.Remover(produ.ID);

                }


            }

            if (e.ColumnIndex == 4)
            {
                ProdutoFornecedorDTO Produtof = dgvproduto.CurrentRow.DataBoundItem as ProdutoFornecedorDTO;

                BlocoCenterAlterarProdutoF tela = new BlocoCenterAlterarProdutoF();
                tela.LoadScreenProduto(Produtof);
                tela.ShowDialog();


            }

        }
    }
}
