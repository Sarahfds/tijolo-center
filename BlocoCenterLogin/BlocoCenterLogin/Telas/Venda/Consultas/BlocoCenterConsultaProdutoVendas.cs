﻿using BlocoCenterLogin.BlocoCenter.Produto;
using BlocoCenterLogin.DTOS;
using BlocoCenterLogin.Telas.Logistica;
using BlocoCenterLogin.Telas.Venda;
using BlocoCenterLogin.Telas.Venda.Compra;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin
{
    public partial class BlocoCenterConsultaProduto : Form
    {
        public BlocoCenterConsultaProduto()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
          BlocoCenterVendasMenu newForm2 = new BlocoCenterVendasMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void btnbuscar_Click(object sender, EventArgs e)
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Consultar(txtpesquisa.Text);



            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 6)
            {
                ProdutoDTO funciona = dataGridView1.CurrentRow.DataBoundItem as ProdutoDTO;

                BlocoCenterAlterarProduto tela = new BlocoCenterAlterarProduto();
                tela.LoadScreen(funciona);
                tela.ShowDialog();


            }
            if (e.ColumnIndex == 5)
            {
                ProdutoDTO funcio = dataGridView1.Rows[e.RowIndex].DataBoundItem as ProdutoDTO;

                DialogResult r = MessageBox.Show("Deseja excluir o cadastro?", "Tijolo Center",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    ProdutoBusiness business = new ProdutoBusiness();
                    business.Remover(funcio.id_produto);

                }
            }
        }
    }
}
