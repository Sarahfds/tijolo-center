﻿using BlocoCenterLogin.BlocoCenter.Produto.ProdutoFornecedor;
using BlocoCenterLogin.DTOS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin.Telas.Venda.Estoque
{
    public partial class BaixaEstoque : Form
    {
        public BaixaEstoque()
        {
            InitializeComponent();
            CarregarComboProduto();
        }

        void CarregarComboProduto()
        {
            ProdutoFornecedorBusiness business = new ProdutoFornecedorBusiness();
            List<ProdutoFornecedorDTO> lista = business.Listar();

            cboProduto.ValueMember = nameof(ProdutoFornecedorDTO.ID);
            cboProduto.DisplayMember = nameof(ProdutoFornecedorDTO.nome);
            cboProduto.DataSource = lista;
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void btnBaixa_Click(object sender, EventArgs e)
        {
            ProdutoFornecedorDTO cat = cboProduto.SelectedItem as ProdutoFornecedorDTO;

            EstoqueDTO dto = new EstoqueDTO();
            dto.id_produto = cat.nome();

        }
    }
}
