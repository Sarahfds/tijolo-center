﻿namespace BlocoCenterLogin.Telas.Venda
{
    partial class BlococCenterEstoqueFornecedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BlococCenterEstoqueFornecedor));
            this.dgvestoquefornecedor = new System.Windows.Forms.DataGridView();
            this.Estoque = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INSS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usuario = new System.Windows.Forms.Label();
            this.txtpesquisa = new System.Windows.Forms.TextBox();
            this.btnbuscar = new System.Windows.Forms.Button();
            this.btnsair = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvestoquefornecedor)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvestoquefornecedor
            // 
            this.dgvestoquefornecedor.AllowUserToAddRows = false;
            this.dgvestoquefornecedor.AllowUserToDeleteRows = false;
            this.dgvestoquefornecedor.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvestoquefornecedor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvestoquefornecedor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Estoque,
            this.INSS});
            this.dgvestoquefornecedor.Location = new System.Drawing.Point(34, 91);
            this.dgvestoquefornecedor.Name = "dgvestoquefornecedor";
            this.dgvestoquefornecedor.ReadOnly = true;
            this.dgvestoquefornecedor.Size = new System.Drawing.Size(1010, 452);
            this.dgvestoquefornecedor.TabIndex = 103;
            // 
            // Estoque
            // 
            this.Estoque.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Estoque.HeaderText = "Produto";
            this.Estoque.Name = "Estoque";
            this.Estoque.ReadOnly = true;
            // 
            // INSS
            // 
            this.INSS.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.INSS.HeaderText = "Quantidade";
            this.INSS.Name = "INSS";
            this.INSS.ReadOnly = true;
            // 
            // usuario
            // 
            this.usuario.AutoSize = true;
            this.usuario.BackColor = System.Drawing.Color.Transparent;
            this.usuario.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.usuario.Location = new System.Drawing.Point(283, 32);
            this.usuario.Name = "usuario";
            this.usuario.Size = new System.Drawing.Size(100, 30);
            this.usuario.TabIndex = 160;
            this.usuario.Text = "Produto:";
            // 
            // txtpesquisa
            // 
            this.txtpesquisa.Location = new System.Drawing.Point(389, 42);
            this.txtpesquisa.Name = "txtpesquisa";
            this.txtpesquisa.Size = new System.Drawing.Size(284, 20);
            this.txtpesquisa.TabIndex = 1;
            this.txtpesquisa.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // btnbuscar
            // 
            this.btnbuscar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnbuscar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnbuscar.Font = new System.Drawing.Font("Goudy Old Style", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnbuscar.ForeColor = System.Drawing.Color.Transparent;
            this.btnbuscar.Location = new System.Drawing.Point(689, 32);
            this.btnbuscar.Name = "btnbuscar";
            this.btnbuscar.Size = new System.Drawing.Size(131, 38);
            this.btnbuscar.TabIndex = 2;
            this.btnbuscar.Text = "Buscar";
            this.btnbuscar.UseVisualStyleBackColor = false;
            // 
            // btnsair
            // 
            this.btnsair.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnsair.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnsair.Font = new System.Drawing.Font("Goudy Old Style", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsair.ForeColor = System.Drawing.Color.Transparent;
            this.btnsair.Location = new System.Drawing.Point(937, 32);
            this.btnsair.Name = "btnsair";
            this.btnsair.Size = new System.Drawing.Size(107, 39);
            this.btnsair.TabIndex = 3;
            this.btnsair.Text = "Voltar";
            this.btnsair.UseVisualStyleBackColor = false;
            this.btnsair.Click += new System.EventHandler(this.btnsair_Click);
            // 
            // BlococCenterEstoqueFornecedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(251)))), ((int)(((byte)(242)))));
            this.ClientSize = new System.Drawing.Size(1077, 575);
            this.Controls.Add(this.btnsair);
            this.Controls.Add(this.btnbuscar);
            this.Controls.Add(this.usuario);
            this.Controls.Add(this.txtpesquisa);
            this.Controls.Add(this.dgvestoquefornecedor);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "BlococCenterEstoqueFornecedor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Estoque Fornecedor";
            ((System.ComponentModel.ISupportInitialize)(this.dgvestoquefornecedor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvestoquefornecedor;
        private System.Windows.Forms.DataGridViewTextBoxColumn Estoque;
        private System.Windows.Forms.DataGridViewTextBoxColumn INSS;
        private System.Windows.Forms.Label usuario;
        private System.Windows.Forms.TextBox txtpesquisa;
        private System.Windows.Forms.Button btnbuscar;
        private System.Windows.Forms.Button btnsair;
    }
}