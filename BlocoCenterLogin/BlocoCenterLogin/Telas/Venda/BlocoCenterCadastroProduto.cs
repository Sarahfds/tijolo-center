﻿using BlocoCenterLogin.BlocoCenter.Fornecedor;
using BlocoCenterLogin.BlocoCenter.Produto;
using BlocoCenterLogin.BlocoCenter.Produto.ProdutoFornecedor;
using BlocoCenterLogin.DTOS;
using BlocoCenterLogin.Telas.Logistica;
using BlocoCenterLogin.Telas.Venda;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin
{
    public partial class BlocoCenterCadastroProduto : Form
    {
        public BlocoCenterCadastroProduto()
        {
            InitializeComponent();
            CarregarCombo();
        }


        void CarregarCombo()
        {
            ProdutoFornecedorBusiness business = new ProdutoFornecedorBusiness();
            List<ProdutoFornecedorDTO> lista = business.Listar();

            cboMaterua.ValueMember = nameof(ProdutoFornecedorDTO.ID);
            cboMaterua.DisplayMember = nameof(ProdutoFornecedorDTO.nome);
            cboMaterua.DataSource = lista;


            FornecedorBusiness funbus = new FornecedorBusiness();
            List<FornecedorDTO> listinha = funbus.Listar();

            cboFornecedor.ValueMember = nameof(FornecedorDTO.id_fornecedor);
            cboFornecedor.DisplayMember = nameof(FornecedorDTO.nome);
            cboFornecedor.DataSource = listinha;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            BlocoCenterConsultaProduto newForm2 = new BlocoCenterConsultaProduto();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            BlocoCenterLogisticaMenu newForm2 = new BlocoCenterLogisticaMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void BlocoCenterCadastroProduto_Load(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {

            BlocoCenterVendasMenu newForm2 = new BlocoCenterVendasMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void btnconsultar_Click(object sender, EventArgs e)
        {
            BlocoCenterConsultaProduto newForm2 = new BlocoCenterConsultaProduto();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void btncadastrar_Click(object sender, EventArgs e)
        {
            FornecedorDTO cat = cboFornecedor.SelectedItem as FornecedorDTO;
            ProdutoFornecedorDTO forn = cboMaterua.SelectedItem as ProdutoFornecedorDTO;


            ProdutoDTO dto = new ProdutoDTO();

            dto.nome = txtNome.Text.Trim();
            dto.preco = Convert.ToDecimal(txtPreco.Text.Trim());           
            dto.id_fornecedor = cat.id_fornecedor;
            dto.id_produtofornecedor = forn.ID;

            ProdutoBusiness business = new ProdutoBusiness();
            business.Salvar(dto);

            MessageBox.Show("Cadastro feito com sucesso.", "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            
        }
    }
}
