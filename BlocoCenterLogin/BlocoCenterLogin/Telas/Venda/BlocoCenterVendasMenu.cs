﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin.Telas.Venda
{
    public partial class BlocoCenterVendasMenu : Form
    {
        public BlocoCenterVendasMenu()
        {
            InitializeComponent();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            BlocoCenterCadastroCliente a = new BlocoCenterCadastroCliente();
            this.Hide();
            a.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            BlocoCenterConsultaCliente a = new BlocoCenterConsultaCliente();
            this.Hide();
            a.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            BlocoCenterVendasPedido a = new BlocoCenterVendasPedido();
            this.Hide();
            a.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            BlocoCenterConsultaVendas a = new BlocoCenterConsultaVendas();
            this.Hide();
            a.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BlocoCenterMenu a = new BlocoCenterMenu();
            this.Hide();
            a.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            BlocoCenterCadastroProduto a = new BlocoCenterCadastroProduto();
            this.Hide();
            a.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            BlocoCenterConsultaProduto a = new BlocoCenterConsultaProduto();
            this.Hide();
            a.ShowDialog();
        }
    }
}
