﻿using BlocoCenterLogin.BlocoCenter.Avaliacao;
using BlocoCenterLogin.DB;
using BlocoCenterLogin.DTOS;
using BlocoCenterLogin.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin
{
    public partial class BlocoCenterAvaliacao : Form
    {
        public BlocoCenterAvaliacao()
        {
            InitializeComponent();
            CarregarCombo();
            CarregarCombo2();
        }
        Validadora a = new Validadora();

        void CarregarCombo()
        {
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = business.Listar();

            cboNome.ValueMember = nameof(FuncionarioDTO.ID);
            cboNome.DisplayMember = nameof(FuncionarioDTO.Nome);
            cboNome.DataSource = lista;
        }

        void CarregarCombo2()
        { 
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = business.Listar();

            cboCargo.ValueMember = nameof(FuncionarioDTO.ID);
            cboCargo.DisplayMember = nameof(FuncionarioDTO.Cargo);
            cboCargo.DataSource = lista;



        }
        private void button1_Click(object sender, EventArgs e)
        {
            BlocoCenterConsultaAvaliacao newForm2 = new BlocoCenterConsultaAvaliacao();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            BlocoCenterMenu newForm2 = new BlocoCenterMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO cat = cboNome.SelectedItem as FuncionarioDTO;
                FuncionarioDTO forn = cboCargo.SelectedItem as FuncionarioDTO;


                AvaliacaoDTO dto = new AvaliacaoDTO();
                dto.avaliador = txtavaliador.Text.Trim();
                dto.Desempenho = cboAvaliacao.Text.Trim();
                dto.DataAvaliacao = DateTime.Now;
                dto.observacao = txtObs.Text.Trim();
                dto.id_funcionario = cat.ID;
                dto.id_funcionario = forn.ID;

                AvaliacaoBusiness business = new AvaliacaoBusiness();
                business.Salvar(dto);
                MessageBox.Show("Pedido salvo com sucesso.", "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {

                MessageBox.Show("Algo deu errado, tente novamente mais tarde", "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnsair_Click(object sender, EventArgs e)
        {
            BlocoCenterRH newForm2 = new BlocoCenterRH();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void btnconsultar_Click(object sender, EventArgs e)
        {

            BlocoCenterConsultaAvaliacao newForm2 = new BlocoCenterConsultaAvaliacao();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO cat = cboNome.SelectedItem as FuncionarioDTO;
                FuncionarioDTO forn = cboCargo.SelectedItem as FuncionarioDTO;


                AvaliacaoDTO dto = new AvaliacaoDTO();
                dto.avaliador = txtavaliador.Text.Trim();
                dto.Desempenho = cboAvaliacao.Text.Trim();
                dto.DataAvaliacao = DateTime.Now;
                dto.observacao = txtObs.Text.Trim();
                dto.id_funcionario = cat.ID;
                dto.id_funcionario = forn.ID;

                AvaliacaoBusiness business = new AvaliacaoBusiness();
                business.Salvar(dto);
                MessageBox.Show("Avaliação salva com sucesso.", "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception)
            {

                MessageBox.Show("Algo deu errado, tente novamente mais tarde", "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void cboNome_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cboCargo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cboAvaliacao_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtavaliador_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Soletras(e);
        }
    }
}
