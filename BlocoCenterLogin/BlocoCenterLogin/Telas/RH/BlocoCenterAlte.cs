﻿using BlocoCenterLogin.DB;
using BlocoCenterLogin.Funcionario;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin.Telas.RH
{
    public partial class BlocoCenterAlte : Form
    {
        public BlocoCenterAlte()
        {
            InitializeComponent();
        }

        FuncionarioDTO Funci;
        Validadora a = new Validadora();

        public void LoadScreen(FuncionarioDTO funciona)
        {
            this.Funci = funciona;

            lbl0.Text = funciona.ID.ToString();


            txtNome.Text = funciona.Nome;
            txtCPF.Text = funciona.CPF;
            txtNumero.Text = funciona.NumeroCasa;
            txtTelefone.Text = funciona.Telefone;
            txtCEP.Text = funciona.CEP;
            dtpNasc.Value = funciona.Nascimento;
            cboSexo.Text = funciona.Sexo;
            txtRG.Text = funciona.RG;
            txtEmail.Text = funciona.Email;
            txtCargo.Text = funciona.Cargo;
            dtpAdmissao.Value = funciona.Admissao;
            txtSalario.Text = funciona.Salario.ToString();
            ckbAdm.Checked = funciona.permissao_adm;
            ckbCompra.Checked = funciona.permissao_compras;
            ckbFinan.Checked = funciona.permissao_financeiro;
            ckbLogistica.Checked = funciona.permissao_logistica;
            funciona.permissao_rh = ckbRH.Checked;
            ckbVenda.Checked = funciona.permissao_vendas;
            txtSenhaa.Text = funciona.Senha;

            
        }

        private void label14_Click(object sender, EventArgs e)
        {

        }


        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            BlocoCenterRH telina = new BlocoCenterRH();
            telina.ShowDialog();
            this.Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Funci.Nome = txtNome.Text.Trim();
            this.Funci.Nome = txtNome.Text.Trim();
            this.Funci.CPF = txtCPF.Text.Trim();
            this.Funci.NumeroCasa = txtNumero.Text.Trim();
            this.Funci.Telefone = txtTelefone.Text.Trim();
            this.Funci.CEP = txtCEP.Text.Trim();
            this.Funci.Nascimento = dtpNasc.Value.Date;
            this.Funci.Sexo = cboSexo.Text;
            this.Funci.Email = txtEmail.Text.Trim();
            this.Funci.Cargo = txtCargo.Text.Trim();
            this.Funci.Admissao = dtpAdmissao.Value.Date;
            this.Funci.Salario = Convert.ToDecimal(txtSalario.Text);
            this.Funci.RG = txtRG.Text.Trim();
            this.Funci.permissao_adm = ckbAdm.Checked;
            this.Funci.permissao_compras = ckbCompra.Checked;
            this.Funci.permissao_financeiro = ckbFinan.Checked;
            this.Funci.permissao_logistica = ckbLogistica.Checked;
            this.Funci.permissao_rh = ckbRH.Checked;
            this.Funci.permissao_vendas = ckbVenda.Checked;
            this.Funci.Senha = txtSenhaa.Text.Trim();

            FuncionarioBusiness business = new FuncionarioBusiness();
            business.Alterar(Funci);
            MessageBox.Show("Cadastro alterado com sucesso");

        }

        private void button2_Click(object sender, EventArgs e)
        {
            BlocoCenterRH tela = new BlocoCenterRH();
            tela.ShowDialog();
            this.Hide ();
        }

        private void btnsair_Click(object sender, EventArgs e)
        {
            BlocoCenterRH tela = new BlocoCenterRH();
           
            this.Hide();
            tela.ShowDialog();
        }

        private void btnalterar_Click(object sender, EventArgs e)
        {
            this.Funci.Nome = txtNome.Text.Trim();
            this.Funci.Nome = txtNome.Text.Trim();
            this.Funci.CPF = txtCPF.Text.Trim();
            this.Funci.NumeroCasa = txtNumero.Text.Trim();
            this.Funci.Telefone = txtTelefone.Text.Trim();
            this.Funci.CEP = txtCEP.Text.Trim();
            this.Funci.Nascimento = dtpNasc.Value.Date;
            this.Funci.Sexo = cboSexo.Text;
            this.Funci.Email = txtEmail.Text.Trim();
            this.Funci.Cargo = txtCargo.Text.Trim();
            this.Funci.Admissao = dtpAdmissao.Value.Date;
            this.Funci.Salario = Convert.ToDecimal(txtSalario.Text);
            this.Funci.RG = txtRG.Text.Trim();
            this.Funci.permissao_adm = ckbAdm.Checked;
            this.Funci.permissao_compras = ckbCompra.Checked;
            this.Funci.permissao_financeiro = ckbFinan.Checked;
            this.Funci.permissao_logistica = ckbLogistica.Checked;
            this.Funci.permissao_rh = ckbRH.Checked;
            this.Funci.permissao_vendas = ckbVenda.Checked;
            this.Funci.Senha = txtSenhaa.Text.Trim();

            FuncionarioBusiness business = new FuncionarioBusiness();
            business.Alterar(Funci);
            MessageBox.Show("Cadastro alterado com sucesso");

        }

        private void button1_Click_1(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void ckbCompra_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void BlocoCenterAlte_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Soletras(e);
        }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtRG_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }

        private void txtCPF_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtTelefone_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void txtCPF_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }

        private void txtTelefone_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }

        private void txtCEP_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }

        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }

        private void txtCargo_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Soletras(e);
        }
    }
}
    

