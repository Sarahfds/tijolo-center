﻿using BlocoCenterLogin.DB;
using BlocoCenterLogin.Funcionario;
using BlocoCenterLogin.Telas.RH;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin
{
    public partial class BlocoCenterConsultaFuncionario : Form
    {
        public BlocoCenterConsultaFuncionario()
        {
            InitializeComponent();
        }
        Validadora a = new Validadora();
        private void button1_Click(object sender, EventArgs e)
        {
           
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            BlocoCenterRH newForm2 = new BlocoCenterRH();
            newForm2.ShowDialog();
            this.Hide();
           
        }

        private void button4_Click(object sender, EventArgs e)
        {





        }

        private void dgvFuncionario_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 14)
            {
                FuncionarioDTO funciona = dgvFuncionario.CurrentRow.DataBoundItem as FuncionarioDTO;

                BlocoCenterAlte tela = new BlocoCenterAlte();
                tela.LoadScreen(funciona);
                tela.ShowDialog();
                

            }
            if (e.ColumnIndex == 13)
            {
                FuncionarioDTO funcio = dgvFuncionario.Rows[e.RowIndex]. DataBoundItem as FuncionarioDTO;

                DialogResult r = MessageBox.Show("Deseja excluir o cadastro?", "Tijolo Center",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    FuncionarioBusiness business = new FuncionarioBusiness();
                    business.Remover(funcio.ID);

                }
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                FuncionarioBusiness business = new FuncionarioBusiness();
                List<FuncionarioDTO> lista = business.Consultar(txtFuncionario.Text);

                dgvFuncionario.AutoGenerateColumns = false;
                dgvFuncionario.DataSource = lista;
            }
            catch (Exception)
            {
                MessageBox.Show("Algo está errado, tente novamente");
            }
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            BlocoCenterRH tela = new BlocoCenterRH();
            
            this.Hide();
            tela.ShowDialog();
        }

        private void BlocoCenterConsultaFuncionario_Load(object sender, EventArgs e)
        {

        }

        private void txtFuncionario_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Soletras(e);
        }
    }
}
