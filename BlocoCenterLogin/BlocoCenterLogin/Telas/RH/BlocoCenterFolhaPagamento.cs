﻿using BlocoCenterLogin.BlocoCenter.FolhadePagamento;
using BlocoCenterLogin.DB;
using BlocoCenterLogin.DTOS;
using BlocoCenterLogin.Funcionario;
using BlocoCenterLogin.Telas.RH;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin
{
    public partial class BlocoCenterFolhaPagamento : Form
    {
        public BlocoCenterFolhaPagamento()
        {
            InitializeComponent();
            CarregarCombos();
        }
        Validadora a = new Validadora();
        void CarregarCombos()
        {
            FuncionarioBusiness business = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = business.Listar();

            cboFuncionario.ValueMember = nameof(FuncionarioDTO.ID);
            cboFuncionario.DisplayMember = nameof(FuncionarioDTO.Nome);
            cboFuncionario.DataSource = lista;

            
            FuncionarioDTO dto = new FuncionarioDTO();
            FuncionarioDTO forn = cboFuncionario.SelectedItem as FuncionarioDTO;
            double SalarioBase = Convert.ToDouble(forn.Salario);

            lblSalariobase.Text = SalarioBase.ToString();


        }


            private void label13_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            BlocoCenterConsultaPagamentoRealizado newForm2 = new BlocoCenterConsultaPagamentoRealizado();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
          

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            BlocoCenterRH newForm2 = new BlocoCenterRH();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void BlocoCenterFolhaPagamento_Load(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
          
  
        }

        private void txtSalarioHora_TextChanged(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txAdicional_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblHoraExtra_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void cboFuncionario_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lblSalarioFamilia_Click(object sender, EventArgs e)
        {

        }

        private void lblAtrasos_Click(object sender, EventArgs e)
        {

        }

        private void lblFaltas_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            BlocoCenterRH newForm2 = new BlocoCenterRH();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void btnsalvar_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO cat = cboFuncionario.SelectedItem as FuncionarioDTO;

                FolhadePagamentoDTO dto = new FolhadePagamentoDTO();

                dto.ValorTotal = Convert.ToDouble(lblTotal.Text);
                dto.ValeTransporte = Convert.ToDouble(lblVale.Text);
                dto.SalarioBase = Convert.ToDouble(lblSalariobase.Text);
                dto.SalarioFamilia = Convert.ToDouble(lblSalarioFamilia.Text);
                dto.id_Funcionario = cat.ID;
                dto.FGTS = Convert.ToDouble(lblFGTS.Text);
                dto.DataPagamento = DateTime.Now;
                dto.INSS = Convert.ToDouble(lblInss.Text);
                dto.HoraExtra = Convert.ToDouble(txtQuant.Text);
                dto.Faltas = Convert.ToDouble(nudFaltas.Text);
                dto.DSR = Convert.ToDouble(lblDSR.Text);
                dto.DomingosTrabalhados = Convert.ToDouble(txtDomingos.Text);
                dto.Diastrabalhados = Convert.ToDouble(txtDias.Text);
                dto.Atrasos = Convert.ToDouble(nudAtrasos.Text);
                dto.IRRF = Convert.ToDouble(lblIRRF.Text);


                FolhadePagamentoBusiness business = new FolhadePagamentoBusiness();
                business.Salvar(dto);

                MessageBox.Show("Folha gerada com sucesso.", "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro tente novamente mais tarde");

            }
        }

        private void btncalcular_Click(object sender, EventArgs e)
        {
            // CONVERSAO //
            try
            {
                double SalarioBase = Convert.ToDouble(lblSalariobase.Text);
                double SalarioHora = Convert.ToDouble(lblSalarioHora.Text);
                double QtdFilhos = Convert.ToDouble(nudFilhos.Text);
                double SalarioFamilia = Convert.ToDouble(lblSalarioFamilia.Text);
                double Atrasos1 = Convert.ToDouble(nudAtrasos.Value);
                double QtdFaltas = Convert.ToDouble(nudFaltas.Value);
                double Adicional = Convert.ToDouble(txtAdic.Text);
                double DiasT = Convert.ToDouble(txtDias.Text);
                double DomingosTrabalhados = Convert.ToDouble(txtDomingos.Text);
                double QtdHorasExtras = Convert.ToDouble(txtQuant.Text);


              



                CalculoFolha a = new CalculoFolha();

                double SH = a.CalcularSH(SalarioBase);
                lblSalarioHora.Text = SH.ToString();

                double hhh = a.CalcularHE(SH, Adicional, QtdHorasExtras);
                lblHoraExtra.Text = hhh.ToString();

                double FGTS = a.CalcularFGTS(SalarioBase);
                lblFGTS.Text = FGTS.ToString();



                double dsr2 = 0;


                double valoratraso = 0;
                if (Atrasos1 > 0)
                {
                    valoratraso = a.CalcularAtrasos(SH, Atrasos1);
                    lblAtrasos.Text = valoratraso.ToString();
                }
                else
                {
                    valoratraso = 0;
                }


                double ValorFaltas = 0;
                if (QtdFaltas > 0)
                {
                    ValorFaltas = a.CalcularFaltas(SalarioBase, QtdFaltas);
                    lblFaltas.Text = ValorFaltas.ToString();
                }


                if (QtdHorasExtras > 0)
                {
                    dsr2 = a.CalcularDsr(DiasT, DomingosTrabalhados, hhh);
                    lblDSR.Text = dsr2.ToString();


                }

                double sf = a.CalcularSF(SalarioBase, QtdFilhos);
                lblSalarioFamilia.Text = sf.ToString();


                double Inss = a.CalcularINSS(SalarioBase, hhh, dsr2, ValorFaltas, valoratraso);
                lblInss.Text = Inss.ToString();

                double Vale = a.CalcularVT(SalarioBase);
                lblVale.Text = Vale.ToString();


                double ir = a.CalcularIR(SalarioBase, Inss);
                lblIRRF.Text = ir.ToString();

                double conta = a.CalcularSalarioLiquido(SalarioBase, hhh, dsr2, ValorFaltas, valoratraso, Inss, Vale, ir);

                lblTotal.Text = conta.ToString();
            }
            catch (Exception)
            {

                MessageBox.Show("Ocorreu um erro tente novamente mais tarde");

            }
        }

        private void btnconsultar_Click(object sender, EventArgs e)
        {
            BlocoCenterConsultaPagamentoRealizado newForm2 = new BlocoCenterConsultaPagamentoRealizado();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void nudAtrasos_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void nudFaltas_ValueChanged(object sender, EventArgs e)
        {

        }

        private void nudFilhos_ValueChanged(object sender, EventArgs e)
        {
           

        }

        private void lblDSR_Click(object sender, EventArgs e)
        {
           

        }

        private void txtDias_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }

        private void txtDomingos_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }

        private void txtQuant_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }

        private void txtAdic_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }
    }
}
