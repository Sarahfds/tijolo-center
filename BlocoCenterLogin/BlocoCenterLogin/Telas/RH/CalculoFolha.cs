﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlocoCenterLogin.Telas.RH
{
    public class CalculoFolha
    {
        public double CalcularSH(double SalarioBase)
        {

            double SH = SalarioBase / 220;
            return SH;
        }

        public double CalcularHE(double SH, double percentual, double quantidade)
        {
            double SalarioHora = SH;
            double ValorHE = SH * (percentual / 100.0);
            double HE = (ValorHE + SH) * quantidade;
            return HE;
        }

        public double CalcularDsr(double DiasT, double DominT, double HE)
        {
            double HoraExtra = HE;


            double Conta1 = HoraExtra / DiasT * DominT;
            return Conta1;
        }


        public double CalcularAtrasos(double SH, double QtdHorasAtrasadas)
        {
            double SalarioHora = SH;
            double Atraso = SalarioHora * QtdHorasAtrasadas;
            return Atraso;
        }

        public double CalcularFaltas(double SalarioBase, double QtdFaltas)
        {
            double SalarioDD = SalarioBase / 30;
            double Faltas =  SalarioDD * QtdFaltas;
            return Faltas;

        }

        public double CalcularINSS(double SalarioBase, double HE, double DSR, double Faltas, double Atrasos)
        {

            double BaseINSS = (SalarioBase + HE + DSR) - (Faltas + Atrasos);
            if (BaseINSS <= 1693.72)
            {
                double INSS1 = BaseINSS * (8 / 100.0);
                return INSS1;
            }
            else if (BaseINSS >= 1693.73 && BaseINSS <= 2822.90)
            {
                double INSS2 = BaseINSS * (9 / 100.0);
                return INSS2;
            }
            else
            {
                double INSS3 = BaseINSS * (11 / 100.0);
                return INSS3;
            }


        }




        public double CalcularFGTS(double SalarioBase)
        {
            double FGTS = SalarioBase * (8 / 100.0);
            return FGTS;
        }

        public double CalcularVT(double SalarioBase)
        {
            double VT = SalarioBase * (6 / 100.0);
            return VT;
        }

        public double CalcularIR(double TotalSalarioBruto, double ValorINSS)
        {
            double PorcentagemIR = 0;
            double valordodesconto = 0;

            TotalSalarioBruto = TotalSalarioBruto - ValorINSS;

            if (TotalSalarioBruto >= 1903.99 && TotalSalarioBruto <= 2826.65)
            {
                PorcentagemIR = 7.5;
                valordodesconto = 142.80;
            }
            if (TotalSalarioBruto >= 2826.66 && TotalSalarioBruto <= 3751.05)
            {
                PorcentagemIR = 15;
                valordodesconto = 354.80;
            }
            if (TotalSalarioBruto >= 3751.06 && TotalSalarioBruto <= 4664.68)
            {
                PorcentagemIR = 22.5;
                valordodesconto = 636.13;
            }
            if (TotalSalarioBruto > 4664.69)
            {
                PorcentagemIR = 27.5;
                valordodesconto = 869.36;
            }

            double valordescontado = TotalSalarioBruto * (PorcentagemIR / 100);
            double IR = valordescontado - valordodesconto;

            return IR;
        }

    

        public double CalcularSF(double SalarioBase, double QuantDeMenor)
        {
           

            if (SalarioBase >= 877.67 && SalarioBase <= 1319.18)
            {
                double SF = QuantDeMenor * 32;
                return SF;
               
            }
            else 
            {
                double SF = QuantDeMenor * 45;
                return SF;

            }
         

        }
            public double CalcularSalarioLiquido(double SalarioBase, double HE, double DSR, double Desc, double Atraso, double INSS, double VT, double IRRF)
            {
                double Total = (SalarioBase + HE + DSR ) - (Desc + Atraso + INSS + VT- IRRF);
                return Total;
            }
        }

           
            }


        

    
        



    

