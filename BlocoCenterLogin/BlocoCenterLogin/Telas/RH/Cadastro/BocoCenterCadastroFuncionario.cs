﻿using BlocoCenterLogin.DB;
using BlocoCenterLogin.Funcionario;
using System;
using System.Windows.Forms;

namespace BlocoCenterLogin
{
    public partial class BlocoCenterCadastroFuncionario : Form
    {
        public BlocoCenterCadastroFuncionario()
        {
            InitializeComponent();
        }
        Validadora v = new Validadora();

     


        private void textBox11_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox13_TextChanged(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Nome = txtNome.Text.Trim();
                dto.CPF = txtCPF.Text.Trim();
                dto.NumeroCasa = txtNumero.Text.Trim();
                dto.Telefone = txtTelefone.Text.Trim();
                dto.CEP = txtCEP.Text.Trim();
                dto.Nascimento = dtpNasc.Value.Date;
                dto.Sexo = cboSexo.Text;
                dto.Email = txtEmail.Text.Trim();
                dto.Cargo = txtCargo.Text.Trim();
                dto.Admissao = dtpAdmissao.Value.Date;
                dto.Salario = Convert.ToDecimal(txtSalario.Text);
                dto.RG = txtRG.Text.Trim();
                dto.permissao_adm = ckbAdm.Checked;
                dto.permissao_compras = ckbCompra.Checked;
                dto.permissao_financeiro = ckbFinan.Checked;
                dto.permissao_logistica = ckbLogistica.Checked;
                dto.permissao_rh = ckbRH.Checked;
                dto.permissao_vendas = ckbVenda.Checked;
                dto.Senha = txtSenhaa.Text.Trim();




                FuncionarioBusiness business = new FuncionarioBusiness();
                business.Salvar(dto);

                MessageBox.Show("Funcionário cadastrado com sucesso.", "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception)
            {

                MessageBox.Show("Algo está errado, tente novamente", "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            BlocoCenterConsultaFuncionario newForm2 = new BlocoCenterConsultaFuncionario();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            BlocoCenterMenu newForm2 = new BlocoCenterMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void BlocoCenterCadastroFuncionario_Load(object sender, EventArgs e)
        {

        }

        private void txtRG_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
         
        }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.Soletras(e);
        }

        private void txtRG_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.Sonumeros(e);
        }

        private void txtCPF_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.Sonumeros(e);
        }

        private void txtTelefone_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.Sonumeros(e);
        }

        private void txtCEP_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.Sonumeros(e);
        }

        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.Sonumeros(e);
        }

        private void txtCargo_KeyPress(object sender, KeyPressEventArgs e)
        {
            v.Soletras(e);
        }

        private void txtSalario_KeyPress(object sender, KeyPressEventArgs e)
        {
          
        }

        private void txtSalario_TextChanged(object sender, EventArgs e)
        {

        }

        private void ckbFinan_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void ckbAdm_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void ckbLogistica_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            BlocoCenterRH newForm2 = new BlocoCenterRH();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            try
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Nome = txtNome.Text.Trim();
                dto.CPF = txtCPF.Text.Trim();
                dto.NumeroCasa = txtNumero.Text.Trim();
                dto.Telefone = txtTelefone.Text.Trim();
                dto.CEP = txtCEP.Text.Trim();
                dto.Nascimento = dtpNasc.Value.Date;
                dto.Sexo = cboSexo.Text;
                dto.Email = txtEmail.Text.Trim();
                dto.Cargo = txtCargo.Text.Trim();
                dto.Admissao = dtpAdmissao.Value.Date;
                dto.Salario = Convert.ToDecimal(txtSalario.Text);
                dto.RG = txtRG.Text.Trim();
                dto.permissao_adm = ckbAdm.Checked;
                dto.permissao_compras = ckbCompra.Checked;
                dto.permissao_financeiro = ckbFinan.Checked;
                dto.permissao_logistica = ckbLogistica.Checked;
                dto.permissao_rh = ckbRH.Checked;
                dto.permissao_vendas = ckbVenda.Checked;
                dto.Senha = txtSenhaa.Text.Trim();




                FuncionarioBusiness business = new FuncionarioBusiness();
                business.Salvar(dto);

                MessageBox.Show("Funcionário cadastrado com sucesso.", "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {

                MessageBox.Show("Algo está errado : " + ex.Message, "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void txtNumero_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
