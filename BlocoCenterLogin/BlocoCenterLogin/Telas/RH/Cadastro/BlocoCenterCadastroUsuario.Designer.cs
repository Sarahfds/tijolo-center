﻿namespace BlocoCenterLogin
{
    partial class BlocoCenterCadastroUsuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BlocoCenterCadastroUsuario));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.usuario = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSenha = new System.Windows.Forms.TextBox();
            this.cbocpf = new System.Windows.Forms.ComboBox();
            this.btncadastrar = new System.Windows.Forms.Button();
            this.rdofinaceiro = new System.Windows.Forms.RadioButton();
            this.rdoadministrador = new System.Windows.Forms.RadioButton();
            this.rdorh = new System.Windows.Forms.RadioButton();
            this.rdologistica = new System.Windows.Forms.RadioButton();
            this.rdocompra = new System.Windows.Forms.RadioButton();
            this.rdovenda = new System.Windows.Forms.RadioButton();
            this.button4 = new System.Windows.Forms.Button();
            this.btnconsultar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(43, 44);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(469, 293);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // usuario
            // 
            this.usuario.AutoSize = true;
            this.usuario.BackColor = System.Drawing.Color.Transparent;
            this.usuario.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.usuario.Location = new System.Drawing.Point(568, 99);
            this.usuario.Name = "usuario";
            this.usuario.Size = new System.Drawing.Size(56, 30);
            this.usuario.TabIndex = 18;
            this.usuario.Text = "CPF:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.label1.Location = new System.Drawing.Point(545, 129);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 30);
            this.label1.TabIndex = 19;
            this.label1.Text = "Senha:";
            // 
            // txtSenha
            // 
            this.txtSenha.Location = new System.Drawing.Point(630, 139);
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.Size = new System.Drawing.Size(374, 20);
            this.txtSenha.TabIndex = 8;
            // 
            // cbocpf
            // 
            this.cbocpf.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbocpf.FormattingEnabled = true;
            this.cbocpf.Location = new System.Drawing.Point(630, 109);
            this.cbocpf.Name = "cbocpf";
            this.cbocpf.Size = new System.Drawing.Size(374, 21);
            this.cbocpf.TabIndex = 7;
            // 
            // btncadastrar
            // 
            this.btncadastrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btncadastrar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btncadastrar.Font = new System.Drawing.Font("Goudy Old Style", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncadastrar.ForeColor = System.Drawing.Color.Transparent;
            this.btncadastrar.Location = new System.Drawing.Point(605, 464);
            this.btncadastrar.Name = "btncadastrar";
            this.btncadastrar.Size = new System.Drawing.Size(132, 44);
            this.btncadastrar.TabIndex = 9;
            this.btncadastrar.Text = "Cadastrar";
            this.btncadastrar.UseVisualStyleBackColor = false;
            this.btncadastrar.Click += new System.EventHandler(this.btnsair_Click);
            // 
            // rdofinaceiro
            // 
            this.rdofinaceiro.AutoSize = true;
            this.rdofinaceiro.BackColor = System.Drawing.Color.Transparent;
            this.rdofinaceiro.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdofinaceiro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.rdofinaceiro.Location = new System.Drawing.Point(605, 186);
            this.rdofinaceiro.Name = "rdofinaceiro";
            this.rdofinaceiro.Size = new System.Drawing.Size(133, 34);
            this.rdofinaceiro.TabIndex = 1;
            this.rdofinaceiro.TabStop = true;
            this.rdofinaceiro.Text = "Financeiro";
            this.rdofinaceiro.UseVisualStyleBackColor = false;
            // 
            // rdoadministrador
            // 
            this.rdoadministrador.AutoSize = true;
            this.rdoadministrador.BackColor = System.Drawing.Color.Transparent;
            this.rdoadministrador.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdoadministrador.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.rdoadministrador.Location = new System.Drawing.Point(605, 226);
            this.rdoadministrador.Name = "rdoadministrador";
            this.rdoadministrador.Size = new System.Drawing.Size(173, 34);
            this.rdoadministrador.TabIndex = 2;
            this.rdoadministrador.TabStop = true;
            this.rdoadministrador.Text = "Administrador";
            this.rdoadministrador.UseVisualStyleBackColor = false;
            // 
            // rdorh
            // 
            this.rdorh.AutoSize = true;
            this.rdorh.BackColor = System.Drawing.Color.Transparent;
            this.rdorh.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdorh.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.rdorh.Location = new System.Drawing.Point(605, 266);
            this.rdorh.Name = "rdorh";
            this.rdorh.Size = new System.Drawing.Size(61, 34);
            this.rdorh.TabIndex = 3;
            this.rdorh.TabStop = true;
            this.rdorh.Text = "RH";
            this.rdorh.UseVisualStyleBackColor = false;
            // 
            // rdologistica
            // 
            this.rdologistica.AutoSize = true;
            this.rdologistica.BackColor = System.Drawing.Color.Transparent;
            this.rdologistica.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdologistica.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.rdologistica.Location = new System.Drawing.Point(605, 306);
            this.rdologistica.Name = "rdologistica";
            this.rdologistica.Size = new System.Drawing.Size(118, 34);
            this.rdologistica.TabIndex = 4;
            this.rdologistica.TabStop = true;
            this.rdologistica.Text = "Logística";
            this.rdologistica.UseVisualStyleBackColor = false;
            // 
            // rdocompra
            // 
            this.rdocompra.AutoSize = true;
            this.rdocompra.BackColor = System.Drawing.Color.Transparent;
            this.rdocompra.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdocompra.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.rdocompra.Location = new System.Drawing.Point(605, 346);
            this.rdocompra.Name = "rdocompra";
            this.rdocompra.Size = new System.Drawing.Size(108, 34);
            this.rdocompra.TabIndex = 5;
            this.rdocompra.TabStop = true;
            this.rdocompra.Text = "Compra";
            this.rdocompra.UseVisualStyleBackColor = false;
            // 
            // rdovenda
            // 
            this.rdovenda.AutoSize = true;
            this.rdovenda.BackColor = System.Drawing.Color.Transparent;
            this.rdovenda.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdovenda.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.rdovenda.Location = new System.Drawing.Point(605, 386);
            this.rdovenda.Name = "rdovenda";
            this.rdovenda.Size = new System.Drawing.Size(93, 34);
            this.rdovenda.TabIndex = 6;
            this.rdovenda.TabStop = true;
            this.rdovenda.Text = "Venda";
            this.rdovenda.UseVisualStyleBackColor = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Font = new System.Drawing.Font("Goudy Old Style", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.Transparent;
            this.button4.Location = new System.Drawing.Point(890, 464);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(114, 44);
            this.button4.TabIndex = 66;
            this.button4.Text = "Sair";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnconsultar
            // 
            this.btnconsultar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnconsultar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnconsultar.Font = new System.Drawing.Font("Goudy Old Style", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnconsultar.ForeColor = System.Drawing.Color.Transparent;
            this.btnconsultar.Location = new System.Drawing.Point(743, 464);
            this.btnconsultar.Name = "btnconsultar";
            this.btnconsultar.Size = new System.Drawing.Size(141, 44);
            this.btnconsultar.TabIndex = 10;
            this.btnconsultar.Text = "Consultar";
            this.btnconsultar.UseVisualStyleBackColor = false;
            this.btnconsultar.Click += new System.EventHandler(this.button5_Click);
            // 
            // BlocoCenterCadastroUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(251)))), ((int)(((byte)(242)))));
            this.ClientSize = new System.Drawing.Size(1076, 584);
            this.Controls.Add(this.btnconsultar);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.rdovenda);
            this.Controls.Add(this.rdocompra);
            this.Controls.Add(this.rdologistica);
            this.Controls.Add(this.rdorh);
            this.Controls.Add(this.rdoadministrador);
            this.Controls.Add(this.rdofinaceiro);
            this.Controls.Add(this.btncadastrar);
            this.Controls.Add(this.cbocpf);
            this.Controls.Add(this.txtSenha);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.usuario);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "BlocoCenterCadastroUsuario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro de Usuário";
            this.Load += new System.EventHandler(this.BlocoCenterCadastroUsuario_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label usuario;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSenha;
        private System.Windows.Forms.ComboBox cbocpf;
        private System.Windows.Forms.Button btncadastrar;
        private System.Windows.Forms.RadioButton rdofinaceiro;
        private System.Windows.Forms.RadioButton rdoadministrador;
        private System.Windows.Forms.RadioButton rdorh;
        private System.Windows.Forms.RadioButton rdologistica;
        private System.Windows.Forms.RadioButton rdocompra;
        private System.Windows.Forms.RadioButton rdovenda;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btnconsultar;
    }
}