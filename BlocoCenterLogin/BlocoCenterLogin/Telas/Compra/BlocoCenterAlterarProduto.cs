﻿using BlocoCenterLogin.BlocoCenter.Fornecedor;
using BlocoCenterLogin.BlocoCenter.Produto;
using BlocoCenterLogin.BlocoCenter.Produto.ProdutoFornecedor;
using BlocoCenterLogin.DTOS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin.Telas.Venda.Compra
{
    public partial class BlocoCenterAlterarProduto : Form
    {
        public BlocoCenterAlterarProduto()
        {
            InitializeComponent();
            CarregarCombo();
        }

        ProdutoDTO pro;

        void CarregarCombo()
        {
            ProdutoFornecedorBusiness business = new ProdutoFornecedorBusiness();
            List<ProdutoFornecedorDTO> lista = business.Listar();

            cboProdutoF.ValueMember = nameof(ProdutoFornecedorDTO.ID);
            cboProdutoF.DisplayMember = nameof(ProdutoFornecedorDTO.nome);
            cboProdutoF.DataSource = lista;


            FornecedorBusiness funbus = new FornecedorBusiness();
            List<FornecedorDTO> listinha = funbus.Listar();

            cboFornecedor.ValueMember = nameof(FornecedorDTO.id_fornecedor);
            cboFornecedor.DisplayMember = nameof(FornecedorDTO.nome);
            cboFornecedor.DataSource = listinha;
        }

        public void LoadScreen(ProdutoDTO produto)
        {
            this.pro = produto;

            lblIf.Text = produto.id_produto.ToString();


            txtNome.Text = produto.nome;
            txtPreco.Text = produto.preco.ToString();
            cboFornecedor.Text = produto.id_fornecedor.ToString();
            cboProdutoF.Text = produto.id_produtofornecedor.ToString();
            
            
           
        }

        private void btncadastrar_Click(object sender, EventArgs e)
        {
            FornecedorDTO forn = cboFornecedor.SelectedItem as FornecedorDTO;
            ProdutoFornecedorDTO cat = cboProdutoF.SelectedItem as ProdutoFornecedorDTO;
            this.pro.id_fornecedor = forn.id_fornecedor;
            this.pro.id_produtofornecedor = Convert.ToInt32(cat.nome);
            this.pro.nome = txtNome.Text.Trim();
            this.pro.preco = Convert.ToInt32(txtPreco.Text.Trim());
          
            ProdutoBusiness business = new ProdutoBusiness();
            business.Alterar(pro);
            MessageBox.Show("Produto alterado com sucesso");
        }

        private void btnsair_Click(object sender, EventArgs e)
        {
            ComprasMenu  a = new ComprasMenu();
            this.Hide();
            a.ShowDialog();
        }
    }
}
