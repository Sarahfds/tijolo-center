﻿using BlocoCenterLogin.BlocoCenter.Cliente;
using BlocoCenterLogin.DB;
using BlocoCenterLogin.DTOS;
using BlocoCenterLogin.Telas.Venda;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin
{
    public partial class BlocoCenterCadastroCliente : Form
    {
        public BlocoCenterCadastroCliente()
        {
            InitializeComponent();
        }
        Validadora a = new Validadora();

        private void button1_Click(object sender, EventArgs e)
        {
            BlocoCenterConsultaCliente newForm2 = new BlocoCenterConsultaCliente();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            BlocoCenterLogistica newForm2 = new BlocoCenterLogistica();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void BlocoCenterCadastroCliente_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            BlocoCenterVendasMenu newForm2 = new BlocoCenterVendasMenu();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void lblNome_Click(object sender, EventArgs e)
        {

        }

        private void usuario_Click(object sender, EventArgs e)
        {

        }

        private void btnconsultar_Click(object sender, EventArgs e)
        {
            BlocoCenterConsultaCliente newForm2 = new BlocoCenterConsultaCliente();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void btncadastrar_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception)
            {

                MessageBox.Show("Algo deu errado, tente novamente mais tarde", "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            ClienteDTO dto = new ClienteDTO();
            dto.nome = txtnome.Text.Trim();
            dto.pessoa = cbotipo.Text;
            dto.rg = mtxrg.Text.Trim();
            dto.telefone = txtTelefone.Text.Trim();
            dto.identificacao = txtcpfcnpj.Text.Trim();

            ClienteBusiness business = new ClienteBusiness();
            business.Salvar(dto);

            MessageBox.Show("Cliente cadastrado com sucesso.", "Tijolo Center", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void txtnome_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Soletras(e);
        }

        private void mtxrg_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }

        private void txtTelefone_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }

        private void txtcpfcnpj_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }
    }
}
