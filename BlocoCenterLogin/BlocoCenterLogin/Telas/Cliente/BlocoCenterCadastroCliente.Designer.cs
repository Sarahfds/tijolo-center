﻿namespace BlocoCenterLogin
{
    partial class BlocoCenterCadastroCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BlocoCenterCadastroCliente));
            this.txtcpfcnpj = new System.Windows.Forms.TextBox();
            this.txtnome = new System.Windows.Forms.TextBox();
            this.lblrg = new System.Windows.Forms.Label();
            this.lblcpfcnpj = new System.Windows.Forms.Label();
            this.lbltelefone = new System.Windows.Forms.Label();
            this.lblnome = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cbotipo = new System.Windows.Forms.ComboBox();
            this.lbltipo = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.btncadastrar = new System.Windows.Forms.Button();
            this.mtxrg = new System.Windows.Forms.MaskedTextBox();
            this.txtTelefone = new System.Windows.Forms.MaskedTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtcpfcnpj
            // 
            this.txtcpfcnpj.Location = new System.Drawing.Point(684, 206);
            this.txtcpfcnpj.Name = "txtcpfcnpj";
            this.txtcpfcnpj.Size = new System.Drawing.Size(334, 20);
            this.txtcpfcnpj.TabIndex = 5;
            this.txtcpfcnpj.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcpfcnpj_KeyPress);
            // 
            // txtnome
            // 
            this.txtnome.Location = new System.Drawing.Point(684, 91);
            this.txtnome.Name = "txtnome";
            this.txtnome.Size = new System.Drawing.Size(334, 20);
            this.txtnome.TabIndex = 1;
            this.txtnome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtnome_KeyPress);
            // 
            // lblrg
            // 
            this.lblrg.AutoSize = true;
            this.lblrg.BackColor = System.Drawing.Color.Transparent;
            this.lblrg.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblrg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblrg.Location = new System.Drawing.Point(630, 111);
            this.lblrg.Name = "lblrg";
            this.lblrg.Size = new System.Drawing.Size(48, 30);
            this.lblrg.TabIndex = 134;
            this.lblrg.Text = "RG:";
            // 
            // lblcpfcnpj
            // 
            this.lblcpfcnpj.AutoSize = true;
            this.lblcpfcnpj.BackColor = System.Drawing.Color.Transparent;
            this.lblcpfcnpj.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcpfcnpj.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblcpfcnpj.Location = new System.Drawing.Point(562, 196);
            this.lblcpfcnpj.Name = "lblcpfcnpj";
            this.lblcpfcnpj.Size = new System.Drawing.Size(117, 30);
            this.lblcpfcnpj.TabIndex = 133;
            this.lblcpfcnpj.Text = "CPF/CNPJ:";
            this.lblcpfcnpj.Click += new System.EventHandler(this.label2_Click);
            // 
            // lbltelefone
            // 
            this.lbltelefone.AutoSize = true;
            this.lbltelefone.BackColor = System.Drawing.Color.Transparent;
            this.lbltelefone.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltelefone.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbltelefone.Location = new System.Drawing.Point(576, 139);
            this.lbltelefone.Name = "lbltelefone";
            this.lbltelefone.Size = new System.Drawing.Size(104, 30);
            this.lbltelefone.TabIndex = 132;
            this.lbltelefone.Text = "Telefone:";
            // 
            // lblnome
            // 
            this.lblnome.AutoSize = true;
            this.lblnome.BackColor = System.Drawing.Color.Transparent;
            this.lblnome.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnome.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lblnome.Location = new System.Drawing.Point(599, 81);
            this.lblnome.Name = "lblnome";
            this.lblnome.Size = new System.Drawing.Size(79, 30);
            this.lblnome.TabIndex = 131;
            this.lblnome.Text = "Nome:";
            this.lblnome.Click += new System.EventHandler(this.usuario_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(33, 23);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(400, 256);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 143;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // cbotipo
            // 
            this.cbotipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbotipo.FormattingEnabled = true;
            this.cbotipo.Items.AddRange(new object[] {
            "Físico",
            "Jurídico"});
            this.cbotipo.Location = new System.Drawing.Point(684, 177);
            this.cbotipo.Name = "cbotipo";
            this.cbotipo.Size = new System.Drawing.Size(334, 21);
            this.cbotipo.TabIndex = 4;
            // 
            // lbltipo
            // 
            this.lbltipo.AutoSize = true;
            this.lbltipo.BackColor = System.Drawing.Color.Transparent;
            this.lbltipo.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltipo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.lbltipo.Location = new System.Drawing.Point(615, 169);
            this.lbltipo.Name = "lbltipo";
            this.lbltipo.Size = new System.Drawing.Size(63, 30);
            this.lbltipo.TabIndex = 155;
            this.lbltipo.Text = "Tipo:";
            this.lbltipo.Click += new System.EventHandler(this.label4_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Font = new System.Drawing.Font("Goudy Old Style", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.Transparent;
            this.button4.Location = new System.Drawing.Point(896, 477);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(122, 50);
            this.button4.TabIndex = 10;
            this.button4.Text = "Voltar";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // btncadastrar
            // 
            this.btncadastrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btncadastrar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btncadastrar.Font = new System.Drawing.Font("Goudy Old Style", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncadastrar.ForeColor = System.Drawing.Color.Transparent;
            this.btncadastrar.Location = new System.Drawing.Point(738, 477);
            this.btncadastrar.Name = "btncadastrar";
            this.btncadastrar.Size = new System.Drawing.Size(152, 50);
            this.btncadastrar.TabIndex = 8;
            this.btncadastrar.Text = "Cadastrar";
            this.btncadastrar.UseVisualStyleBackColor = false;
            this.btncadastrar.Click += new System.EventHandler(this.btncadastrar_Click);
            // 
            // mtxrg
            // 
            this.mtxrg.Location = new System.Drawing.Point(684, 121);
            this.mtxrg.Mask = "00.000.000-0";
            this.mtxrg.Name = "mtxrg";
            this.mtxrg.Size = new System.Drawing.Size(334, 20);
            this.mtxrg.TabIndex = 2;
            this.mtxrg.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.mtxrg_KeyPress);
            // 
            // txtTelefone
            // 
            this.txtTelefone.Location = new System.Drawing.Point(684, 149);
            this.txtTelefone.Mask = "(00)00000-0000";
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(334, 20);
            this.txtTelefone.TabIndex = 3;
            this.txtTelefone.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.maskedTextBox1_MaskInputRejected);
            this.txtTelefone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTelefone_KeyPress);
            // 
            // BlocoCenterCadastroCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(251)))), ((int)(((byte)(242)))));
            this.ClientSize = new System.Drawing.Size(1076, 583);
            this.Controls.Add(this.txtTelefone);
            this.Controls.Add(this.mtxrg);
            this.Controls.Add(this.btncadastrar);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.lbltipo);
            this.Controls.Add(this.cbotipo);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtcpfcnpj);
            this.Controls.Add(this.txtnome);
            this.Controls.Add(this.lblrg);
            this.Controls.Add(this.lblcpfcnpj);
            this.Controls.Add(this.lbltelefone);
            this.Controls.Add(this.lblnome);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "BlocoCenterCadastroCliente";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro de Clientes";
            this.Load += new System.EventHandler(this.BlocoCenterCadastroCliente_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtcpfcnpj;
        private System.Windows.Forms.TextBox txtnome;
        private System.Windows.Forms.Label lblrg;
        private System.Windows.Forms.Label lblcpfcnpj;
        private System.Windows.Forms.Label lbltelefone;
        private System.Windows.Forms.Label lblnome;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox cbotipo;
        private System.Windows.Forms.Label lbltipo;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button btncadastrar;
        private System.Windows.Forms.MaskedTextBox mtxrg;
        private System.Windows.Forms.MaskedTextBox txtTelefone;
    }
}