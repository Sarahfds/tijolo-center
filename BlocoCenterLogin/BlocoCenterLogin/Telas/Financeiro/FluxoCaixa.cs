﻿using BlocoCenterLogin.BlocoCenter.Financeiro;
using BlocoCenterLogin.BlocoCenter.FluxodeCaixa;
using BlocoCenterLogin.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlocoCenterLogin.Telas.Financeiro
{
    public partial class FluxoCaixa : Form
    {
        public FluxoCaixa()
        {
            InitializeComponent();


           
        }

        Validadora a = new Validadora();



        private void mtxdata1_KeyPress(object sender, KeyPressEventArgs e)
        {
            a.Sonumeros(e);
        }

        private void mtxdata1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }


        private void dgvfluxo_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        

        private void dtpinicio_ValueChanged(object sender, EventArgs e)
        {
        }

        private void dtpfim_ValueChanged(object sender, EventArgs e)
        {
        }

        private void btnsair_Click(object sender, EventArgs e)
        {
            BlocoCenterMenu sair = new BlocoCenterMenu();
            this.Hide();
            sair.ShowDialog();
        }

        private void FluxoCaixa_Load(object sender, EventArgs e)
        {

        }

        private void btncalcular_Click(object sender, EventArgs e)
        {
            FluxodeCaixaBusiness business = new FluxodeCaixaBusiness();
            List<FluxoCaixaView> lista = business.Consultar(txtInicio.Text, txtFim.Text);

            dgvfluxo.AutoGenerateColumns = false;
            dgvfluxo.DataSource = lista;
        }
    }
}
